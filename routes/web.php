<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Frontend ///////////////////////////////////////

// Home page
Route::get('/', 'HomePageController@index')->name('home');
Route::get('/twitterUserTimeLine', 'TwitterController@twitterUserTimeLine');
Route::post('tweet', ['as'=>'post.tweet','uses'=>'TwitterController@tweet']);
// blog page
Route::get('/blog', 'HomePageController@blog')->name('blog');

// Backend ///////////////////////////////////////

Route::get('/admin-panel', 'Backend\AllPurposeController@login')->name('backendLogin');
Route::post('/admin-panel', 'Backend\AllPurposeController@doLogin');
Route::get('/admin-panel/logout', 'Backend\AllPurposeController@logout');

Route::middleware('backendCheck')->prefix('backend')->group(function(){
Route::get('/index', 'Backend\AllPurposeController@index')->name('backendIndex');

	
	//  pages
Route::get('/pages', 'Backend\PagesController@index');
Route::get('/add-page', 'Backend\PagesController@add');
Route::post('/add-page', 'Backend\PagesController@store');
Route::post('/update-page/{id}', 'Backend\PagesController@update');
Route::get('/update-page/{id}', 'Backend\PagesController@edit');
Route::get('/pages/{id}','Backend\PagesController@updateStatus');
Route::get('/add-home-page', 'Backend\PagesController@addHomePage');
Route::post('/add-home-page', 'Backend\PagesController@storeHomePage');
;

    
   //builder
Route::get('/builders','Backend\BuildersController@index');
Route::get('/add-builder','Backend\BuildersController@add');
Route::get('/builders/{id}','Backend\BuildersController@updateBuilderStatus');
Route::post('/add-builder','Backend\BuildersController@store');
Route::post('/edit-builder/{id}','Backend\BuildersController@store');

Route::get('/builders/{id}','Backend\BuildersController@updateBuilderStatus');
Route::get('/update-builder/{id}', 'Backend\BuildersController@edit');
Route::post('/update-builder/{id}', 'Backend\BuildersController@update');


//category
  Route::get('/categories','Backend\CategoriesController@index');
Route::get('/categories/{id}','Backend\CategoriesController@updateStatus');
Route::get('/add-category','Backend\CategoriesController@add');
Route::post('/add-category','Backend\CategoriesController@store');
Route::get('/update-category/{id}','Backend\CategoriesController@edit');
Route::post('/update-category/{id}','Backend\CategoriesController@update');
   
   //Location
 Route::get('/locations','Backend\LocationsController@index');
Route::get('/locations/{id}','Backend\LocationsController@updateStatus');
  Route::get('/add-locations','Backend\LocationsController@add');
   Route::post('/add-locations','Backend\LocationsController@store');
Route::get('/update-location/{id}','Backend\LocationsController@edit');
Route::post('/update-location/{id}','Backend\LocationsController@update');

    //projects
  Route::get('/projects','Backend\ProjectsController@index');
  Route::get('/projects/bid={bid}','Backend\ProjectsController@builderProjects');
  
   Route::get('/add-projects','Backend\ProjectsController@add');
    Route::get('/add-projects/{b_id}','Backend\ProjectsController@add');
    Route::post('/add-projects/{b_id}','Backend\ProjectsController@storeBuilderProject');
   Route::post('/add-projects','Backend\ProjectsController@store');
   Route::get('/update-projects/{id}','Backend\ProjectsController@edit');
  Route::get('/projects/{id}','Backend\ProjectsController@updateProjectStatus');
 
   Route::post('/update-projects/{id}','Backend\ProjectsController@update');
  //amenities
      Route::get('/amenities','Backend\AmenitiesController@index');
Route::get('/amenities/{id}','Backend\AmenitiesController@updateStatus');
Route::get('/update-amenity/{id}','Backend\AmenitiesController@add');
Route::get('/update-amenity/{id}','Backend\AmenitiesController@edit');
Route::post('/update-amenity/{id}','Backend\AmenitiesController@update');
  Route::get('/add-amenity','Backend\AmenitiesController@add');
 Route::post('/add-amenity','Backend\AmenitiesController@store');



  Route::get('/add-amenity','Backend\AmenitiesController@add');
 Route::post('/add-amenity','Backend\AmenitiesController@store');
//property type
  Route::get('/prop-type','Backend\PropertyTypesController@index');
Route::get('/prop-type/{id}','Backend\PropertyTypesController@updateStatus');
Route::get('/add-prop-type','Backend\PropertyTypesController@add');
Route::post('/add-prop-type','Backend\PropertyTypesController@store');
Route::get('/update-prop-type/{id}','Backend\PropertyTypesController@edit');
Route::post('/update-prop-type/{id}','Backend\PropertyTypesController@update');
 Route::get('/update-property/{id}','Backend\PropertiesController@edit');
 Route::post('/update-property/{id}','Backend\PropertiesController@update');

//Property
  Route::get('/properties','Backend\PropertiesController@index');
 Route::get('/add-property','Backend\PropertiesController@add');
 Route::post('/add-property','Backend\PropertiesController@store');
 Route::get('/properties/{id}','Backend\PropertiesController@updateStatus');

//Property Prices
 
Route::get('/add-prop-price','Backend\PropertyPricesController@index');
Route::post('/add-prop-price','Backend\PropertyPricesController@store');

 Route::get('/add-property','Backend\PropertiesController@add');
 ///// sUBDOMAINS////////////////////
  Route::get('/subdomains','Backend\SubdomainsController@index');
  Route::get('/add-subdomain','Backend\SubdomainsController@add');
  Route::post('/add-subdomain','Backend\SubdomainsController@store');
  Route::get('/add-subdomain-projects/{id}','Backend\SubdomainsController@addProject');
  Route::post('/add-subdomain-projects/{id}','Backend\SubdomainsController@storeProject');
 Route::get('/subdomain-projects/{id}','Backend\SubdomainsController@getProject');

  Route::get('/ajaxLocBuilder/{id}','Backend\PropertiesController@ajaxLocBuilder');
  Route::get('/ajaxBuilder/{id}','Backend\ProjectsController@ajaxBuilder');
  Route::get('/ajaxProject/{id}','Backend\PropertiesController@ajaxProject');

 Route::get('/ajaxBuilderProject/{id}','Backend\PropertiesController@ajaxBuilderProject');
 Route::get('/deletePropImg/{id}/{propId}','Backend\PropertiesController@deletePropImg');
Route::get('/deletePropf_Img/{id}/{propId}','Backend\PropertiesController@deletePropf_img');

///// sUBDOMAINS////////////////////
Route::get('/subdomains','Backend\SubdomainsController@index');
  Route::get('/add-subdomain','Backend\SubdomainsController@add');
  Route::post('/add-subdomain','Backend\SubdomainsController@store');
  Route::get('/add-subdomain-projects/{id}','Backend\SubdomainsController@addProject');
  Route::post('/add-subdomain-projects/{id}','Backend\SubdomainsController@storeProject');
 Route::get('/subdomain-projects/{id}','Backend\SubdomainsController@getProject');
  Route::get('/update-subdomain-projects/{id}','Backend\SubdomainsController@editproject');
  Route::post('/update-subdomain-projects/{id}','Backend\SubdomainsController@updateproject');
  Route::get('/update-subdomain/{id}','Backend\SubdomainsController@edit');
  Route::post('/update-subdomain/{id}','Backend\SubdomainsController@update');
Route::get('/subdomains/{id}','Backend\SubdomainsController@updateSubdomainStatus');
//////subdomain price///////////
Route::get('/add-subdomain-price','Backend\SubdomainsController@getSubdomains');
Route::post('/add-subdomain-price','Backend\SubdomainsController@addSubdomainPrice');


////////////////USER PROFILE////////////////
Route::get('/edit-profile/{id}','Backend\UserController@edit');
Route::post('/edit-profile/{id}','Backend\UserController@update');
Route::get('/change-password/{id}','Backend\UserController@editPassword');
Route::post('/change-password/{id}','Backend\UserController@updatePassword');

////////////////Property Specifications////////////////
Route::get('/specifications','Backend\SpecificationsController@index');
Route::get('/add-specifications','Backend\SpecificationsController@add');
Route::post('/add-specifications','Backend\SpecificationsController@store');
Route::get('/add-feature/{id}','Backend\SpecificationsController@addFeature');
Route::post('/add-feature/{id}','Backend\SpecificationsController@storeFeature');
Route::get('/view-feature/{id}','Backend\SpecificationsController@viewFeature');
Route::get('/update-specification/{id}','Backend\SpecificationsController@edit');
Route::post('/update-specification/{id}','Backend\SpecificationsController@updateSpecification');
Route::get('/update-feature/{id}','Backend\SpecificationsController@editFeature');
Route::post('/update-feature/{id}','Backend\SpecificationsController@updateFeature');
////////////////HOME LOAN////////////////////////
Route::get('/home-loan','Backend\HomeLoanController@index');
});  








