
<div class="clearfix"> </div>

<!-- Section footer -->
<footer id="footer">
  <div class="container">
    <div class="footer-one">
      <div class="row">
        <div class="col-sm-12 col-md-3">
          <div class="footer-logo">
            <h4> Mera Rera</h4>
          </div>
          <p> <span class="copyright">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the...</span> </p>
          <div class="social-outer">
            <div class="footer-logo">
              <h4>Get Socialize</h4>
            </div>
            <p>
            	<a href="https://www.facebook.com/meraRERAIndia/" target="_blank">
            		<i class="fa fa-facebook sizeF" aria-hidden="true"></i>
            	</a>
            	<a href="https://plus.google.com/u/0/b/110370531070407430520/110370531070407430520/about/p/pub" target="_blank">
            		<i class="fa fa-google-plus sizeF leftF" aria-hidden="true"></i>
            	</a>
				<a href="https://twitter.com/RERAIndia" target="_blank">
            		<i class="fa fa-twitter sizeF leftF" aria-hidden="true"></i>
            	</a>
            </p>
          </div>
        </div>
        <div class="col-sm-12 col-md-3">
          <h4> Our Links </h4>
          <ul class="menu hide-bullets nopadding">
            <li> <a href="{{ url('/') }}"> Home </a> </li>
            <li> <a href="{{ url('/page/about-us') }}/"> meraRERA </a> </li>
            <li> <a href="#"> Why RERA? </a> </li>
            <li> <a href="#"> RERA For All </a> </li>
            <li> <a href="#"> FAQs </a> </li>
            <li> <a href="{{ url('/blogs') }}/"> Blogs </a> </li>
            <li> <a href="{{ url('/contact') }}/"> Contact Us </a> </li>
          </ul>
        </div>
        <div class="col-sm-12 col-md-3">
          <h4> Help And Support </h4>
          <ul class="menu hide-bullets nopadding">
            <li> <a href="#"> For Buyers </a> </li>
            <li> <a href="#"> For Real Estate Agents </a> </li>
            <li> <a href="#"> For Promoters </a> </li>
            <li> <a href="{{ url('/page/terms-condition') }}/"> Term and Conditions </a> </li>
            <li> <a href="{{ url('/page/privacy-policy') }}/"> Pravicy Policy </a> </li>
          </ul>
        </div>
        <div class="col-sm-12 col-md-3">
          <h4> Contact Info </h4>
          <div class="wrap-newsletter">
            <p> <i class="fa fa-map-marker marginR10" aria-hidden="true"></i>425 South Medical Dr. Suite 212 
              Bountiful, UT 84010<br>
              <br>
              <i class="fa fa-phone marginR10" aria-hidden="true"></i> Phone: +91-9810521407 </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-two">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="text-center"> Copyright &copy; 2017 <strong> MeraRera.com </strong> — <a class="copyright" href="#"> MeraRera </a> | Digital All Rights Reserved. </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- End of Section footer -->

<!-- START JAVASCRIPT --> 
<script type="text/javascript" src="{{ asset('public/js/jquery.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/wow.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/shuffleLetters.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/owl.caraousel.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/bootsnav.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/chosen.jquery.js') }}"></script> 

<script type='text/javascript' src="{{ asset('/public') }}/js/html5map_js/jquery.tipsy.js"></script>
<script type='text/javascript' src="{{ asset('/public') }}/js/html5map_js/chosen/chosen.jquery.js"></script>
<script type='text/javascript' src="{{ asset('/public') }}/js/html5map_js/chosen/chosen.proto.min.js"></script>



<style>body .fm-tooltip { color: #000000; font-size: 20px; }</style>
<script src='//cdn.html5maps.com/3d_party/raphael.min.js'></script>
<script src="{{ asset('/public') }}/js/html5map_js/state_detail.js"></script>
<script src='//cdn.html5maps.com/libs/locator/2.4.0/india/map.js'></script>

<script>
	var baseUrl = $('head base').attr('href');
	var defaultState = "{{ session()->get('qryParams.defaultState') }}";

	function getStates() {
		var url = 'get-states';
		var dataObj = {};

		var resultCallback = function(states) {
			var counter = 1;
			var stateList = "<div class='col-menu col-md-2'><div class='content'><ul class='menu-col'>";
			
			$.each(states, function( index, element ) {
				stateList += "<li><a href='rera-" + element.name.replace(/ /g, '-').toLowerCase() + "'>" + element.name + "</a></li>";

				if( counter % 6 == 0 ) {
					stateList += "</ul></div></div><div class='col-menu col-md-2'><div class='content'><ul class='menu-col'>";
				}

				counter++;
			});

			stateList += "</ul></div></div>";

			$( "#allStates" ).html( stateList );
		}

		ajaxCal( $( "#allStates" ), url, dataObj, resultCallback );
	}

	function loadStateBuilders( stateId ) {
		$( "#builderList" ).html("");

		var url = 'builder';
		var dataObj = { stateId: stateId };
		var resultCallback = function(builders) {
			var options = "<option value=''>Select Builder</option>";

			$.each(builders, function( index, element ) {
				options += "<option value='" + element.id + "'>" + element.name + "</option>";
			});

			$( "#builderList" ).html( options );
		}

		ajaxCal( $( "#stateBuilders" ), url, dataObj, resultCallback );
	}


    function loadBuilderProjects(stateId, builderId) {
		$( "#projectList" ).html( "" );

    	var url = 'project';
    	var dataObj = {stateId: stateId, builderId: builderId};
		var resultCallback = function(projects) {
			var options = "<option value=''>Select Project</option>";

			$.each(projects, function( index, element ) {
				options += "<option value='" + element.id + "'>" + element.name + "</option>";
			});

			$( "#projectList" ).html( options );
		}

    	ajaxCal( $( "#builderProjects" ), url, dataObj, resultCallback );
    }


	function ajaxCal(loadingObj, url, dataObj, resultFunc='', loadingMsg='', type='POST') {

    	showLoader(loadingObj, loadingMsg);

    	// If resultFunc is not sent, than display result
    	// data on loader place.
    	if ( resultFunc == '' ) {
    		var resultFunc = function( result ){
				loadingObj.html( result );

    			hideLoader(loadingObj);
			}
    	}

		// Adding token to post data
		dataObj._token = $('input[name=_token]').val();
    	$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: type,
			url: baseUrl + '/' + url,
			data: dataObj,
			success: resultFunc
		});

		hideLoader(loadingObj);
    }


	function showLoader(obj, msg="", cls="") {
		if( msg == "" ) {
			msg = "Loading...";
		}

		obj.hide();
		obj.before("<div id='loader' class='" + cls + "' style='font-style:italic;'>" + msg + "</div>");
	}


	function hideLoader(obj) {
		$("#loader").remove();
		obj.show();
	}


	$(document).ready(function(e) {
		/**
		 * Load default state detail
		 * In our case it is - New Delhi (st34)
		 */
		if( ! defaultState ) {
			defaultState = 'st34';
		}
		//$( "#stateHighlights" ).html(map_cfg.map_data['st34']['name']);
		$( "#stateHighlights" ).html(map_cfg.map_data[defaultState]['name']);
        $( "#stateHighlightsInner" ).html(map_cfg.map_data[defaultState]['comment']);
        $( "#detailedQuery" ).focus();

        getStates();

        // Load builder
        loadStateBuilders(defaultState);

        // Load projects on selecting builder
        $( "#builderList" ).on( "change", function() {
        	loadBuilderProjects(defaultState, this.value);
        });


        $("#closemodal").click(function(){
		    $("#myModal").removeClass("in");
		    $("#myModal").css("display","none");
		    $(".modal-backdrop").remove();
		    $("body").removeClass("modal-open");
		    $("body").css("padding-right","");
		    $("#registerBox1").show();
		    $("#registerBox2").hide();
		    $("#msg").html("");
		    $("#registerBox1, #registerBox2").find('input:text').val('');
		});

		var qry = '';
		var qryLength = 0;
		var maxLength = 600;
		var legalQryObj = $('#detailedQuery');
		var remainingLength

		// Trim query to limited length
		legalQryObj.on('keyup', function() {
			qry = $(this).val();
			qryLength = qry.length;
			remainingLength = maxLength - qryLength;

			if ( qryLength > maxLength ) {
				$(this).val(qry.slice(0, maxLength));
				remainingLength = 0;
			}

			$( "#charsRemaining" ).html('Characters remaining: ' + remainingLength);
		});

		$('#stepTwoLegalQuery').on('click', function(){
			if( legalQryObj.val() == '' ) {
				alert('Please write your legal query.');
				legalQryObj.focus();
				return false;
			} else if( qryLength > maxLength ) {
				alert('Query length cannot be more than 600 characters.');
				legalQryObj.val(qry.slice(0, maxLength));
				legalQryObj.focus();
				return false;
			}

			$( "#selectedStateHeading" ).html(map_cfg.map_data[defaultState]['name']);

	  		$('#legalQueryBlk').animate({ "left": "-=500px" }, "fast", function(){
	  			$(this).hide();
	  			$("#builderAndProject").show();
	  		});
		});

		$('#editLegalQuery').on('click', function(){
  			$( "#legalQueryBlk" ).show();
	  		$( "#builderAndProject" ).hide();
			$( "#detailedQuery" ).focus();
  			$( "#legalQueryBlk" ).animate({ "left": "+=500px" }, "fast", function(){
	  		});

  			/*$("#legalQueryBlk").css({ "left": "+=500px" });
	  		$("#builderAndProject").animate({ "left": "675px" }, "fast", function(){
  				$("#legalQueryBlk").show();
		  		$("#builderAndProject").hide().css({ "left": "34px" });
	  		});*/
		});

		/* Add new builder starts */
		$("#addNewBuilder").on("click", function() {
			$( "#stateBuilders" ).prev().hide();
			$(this).add( "#stateBuilders" ).hide();
			$("#newBuilder, #saveRemoveNewBuilderBlk").show();
			$("#newBuilder").focus();
		});

		/*$("#saveNewBuilder").on("click", function() {
			showLoader($("#saveRemoveNewBuilderBlk"), "Saving data...", "wheat-bg-clr");
			$("#newBuilder").hide();
			alert($("#newBuilder").val());
		});*/

		$("#removeNewBuilder").on("click", function() {
			//$("#newBuilder, #save_").add(this).hide();
			$("#newBuilder, #saveRemoveNewBuilderBlk").hide();
			$("#stateBuilders, #addNewBuilder").show();
			$("#newBuilder").val('');
			$("#builderList").focus();
		});
		/* Add new builder ends */


		/* Add new project starts */
		$("#addNewProject").on("click", function() {
			$( "label" ).attr( "for", "projectList" ).hide();
			$(this).add( "#builderProjects" ).hide();
			$("#newProject, #saveRemoveNewProjectBlk").show();
			$("#newProject").focus();
		});

		/*$("#saveNewProject").on("click", function() {
			showLoader($("#saveRemoveNewProjectBlk"), "Saving data...", "wheat-bg-clr");
			$("#newProject").hide();
			alert($("#newProject").val());
		});*/

		$("#removeNewProject").on("click", function() {
			//$("#newProject, #save_").add(this).hide();
			$("#newProject, #saveRemoveNewProjectBlk").hide();
			$("#builderProjects, #addNewProject").show();
			$("#newProject").val('');
			$("#projectList").focus();
		});
		/* Add new project ends */




		$( "#signInLegalQuery" ).on( "click", function() {
			if( ! $( "#builderList" ).val() ) {
				alert("Please select builder");
				return false;
			}
			if( ! $( "#projectList" ).val() ) {
				alert("Please select project");
				return false;
			}

			$( "#myModal" ).on('shown.bs.modal', function() {
			  $('#signInEmail').focus();
			})
		});


		var msgDiv = '<div class="form-group"><div id="msg" class="" role="alert"></div></div>';

		$( "#signInBtn" ).on( "click", function() {
			var url = 'signin';
			var em = $( "#signInEmail" ).val();
			var pwd = $( "#signInPassword" ).val();
			var rm = $( "#rememberme" ).val();
			var signInRespMsg = '';

			if( ! em ) {
				alert("Email is required.");
				$( "#signInEmail" ).focus();
				return false;
			}
			if( ! pwd ) {
				alert("Password is required.");
				$( "#signInPassword" ).focus();
				return false;
			}

			regObj = {
				email: em, 
				password: pwd, 
				rememberMe: rm,
				legalQry: legalQryObj.val()
			};

			$( ".login-popup-order" ).hide();
			$( "#registerBoxLoaderMsg" ).html("<p><i>Validating credentials...</i></p>");
			$( "#registerBoxLoader" ).show();

	    	// Adding token to post data
			regObj._token = $('input[name=_token]').val();
	    	$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				url: url,
				data: regObj,
				success: function(response) {
					if( response.status ) {
						//$('#modal').modal('hide');
						window.location = "{{ URL::to('/') }}";
					} else {
						signInRespMsg = response.message;
						signInRespLastStep();
					}
				},
				error: function (reject) {
				    if( reject.status === 422 ) {
				        var respError = $.parseJSON(reject.responseText);
				        $.each(respError.errors, function (key, val) {
				        	if ( val !== null && typeof val === 'object' ) {
				        		signInRespMsg = val;
								signInRespLastStep();
				        	}
				        });
				    }
				}
			});
			
			signInRespLastStep = function() {
				if ( ! $( "#msg" ).length ) {
					$( "#signIn" ).prepend( '<div style="margin: 15px;"><div class="form-group"><div id="msg" class="alert alert-danger" role="alert"></div></div></div>' );
				}

				$( "#msg" ).html( signInRespMsg );
				$( "#registerBoxLoader" ).hide();
				$( ".login-popup-order" ).show();

				setTimeout( function() { 
					$( "#msg" ).slideUp( "fast" );
				}, 4000);
				setTimeout( function() { 
					$( "#msg" ).remove();
				}, 4300);
				
				return false;
			}
		});


		$( "#sendOtp, #resendOtp" ).on( "click", function() {
			var url = 'send-otp';
			var fn = $( "#fullname" ).val();
			var em = $( "#email" ).val();
			var ph = $( "#phone" ).val();

			if( ! fn ) {
				alert("Full name is required.");
				$( "#fullname" ).focus();
				return false;
			}
			if( ! em ) {
				alert("Email is required.");
				$( "#email" ).focus();
				return false;
			}
			if( ! ph ) {
				alert("Mobile number is required.");
				$( "#phone" ).focus();
				return false;
			}

			regObj = { name: fn, email: em, phone: ph };

			$( ".login-popup-order" ).hide();
			$( "#registerBoxLoaderMsg" ).html("<p><i>Sending OTP...</i></p>");
			$( "#registerBoxLoader" ).show();

	    	// Adding token to post data
			regObj._token = $('input[name=_token]').val();
	    	$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				url: url,
				data: regObj,
				success: function(response) {
					var alertBgClr = "";
					if( response.status ) {
						alertBgClr = "alert alert-success";
					} else {
						alertBgClr = "alert alert-danger";
					}

					if ( ! $( "#msg" ).length ) {
						$( "#signUp" ).prepend( msgDiv );
					}

					$( "#msg" ).removeClass();
					$( "#msg" ).addClass(alertBgClr);
					$( "#msg" ).html( response.message );
					$( "#registerBoxLoader, #registerBox1" ).hide();
					$( ".login-popup-order, #registerBox2" ).show();
					$( "#otp" ).focus();
				},
				error: function (reject) {
				    if( reject.status === 422 ) {
				        var respError = $.parseJSON(reject.responseText);
				        $.each(respError.errors, function (key, val) {
				        	if ( val !== null && typeof val === 'object' ) {
								var alertBgClr = "alert alert-danger";

								if ( ! $( "#msg" ).length ) {
									$( "#signUp" ).prepend( "<div style='margin: 15px;'>" + msgDiv + "</div>" );
									$( "#msg" ).addClass(alertBgClr);
								}

								$( "#msg" ).html( val );
								$( "#registerBoxLoader" ).hide();
								$( ".login-popup-order, #registerBox1" ).show();

								return false;
				        	}
				        });
				    }
				}
			});
		});

		// Verify OTP
		$( "#verifyOtp" ).on( "click", function(){
			if( ! $( "#otp" ).val() ) {
				alert("OTP is required.");
				$( "#otp" ).focus();
				return false;
			}

			$( ".login-popup-order" ).hide();
			$( "#registerBoxLoaderMsg" ).html("<p><i>Verifying OTP...</i></p>");
			$( "#registerBoxLoader" ).show();

	    	// Adding otp & token to post data
			var dataObj = { 
				name: regObj.name,
				email: regObj.email,
				password: regObj.phone,
				phone: regObj.phone,
				otp: $( "#otp" ).val(),
				legalQry: legalQryObj.val(),
				_token: $('input[name=_token]').val() 
			};

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				url: 'verify-otp',
				data: dataObj,
				success: function(response) {
					if( response.status ) {
						alertBgClr = "alert alert-success";
					} else {
						alertBgClr = "alert alert-danger";
					}

					$( "#msg" ).addClass(alertBgClr);

					if ( ! $( "#msg" ).length ) {
						$( "#signUp" ).prepend( msgDiv );
					} else {
						$( "#msg" ).removeClass();
						$( "#msg" ).addClass(alertBgClr);
					}

					$( "#msg" ).html( response.message );
					$( "#registerBoxLoader, #registerBox1" ).hide();
					$( ".login-popup-order, #registerBox2" ).show();

					if( response.status ) {
						//$('#modal').modal('hide');
						window.location = "{{ URL::to('/') }}";
					}
				}
			});
		});


		$( "#paymentLegalQuery" ).on( "click", function(){
			var url = 'doPayment';
			var builderId = $( "#builderList" ).val();
			var projectId = $( "#projectList" ).val();
			var dataObj = { legalQry: legalQryObj.val(), builderId: builderId, projectId: projectId };

			var resultCallback = function(response) {
				if( response.status ) {
					window.location = "{{ url('/paymentform') }}";
					//window.location = "https://www.payumoney.com/paybypayumoney/#/A3E42A4EA5383FF1F4C420979C62BE9B";
				}
			}

			ajaxCal( $( "#stateBuilders" ), url, dataObj, resultCallback );
		});

	});
</script>

	<!-- Custom  js--> 
	 
	<!-- End JAVASCRIPT -->
