<!-- Section Top Header -->
<div class="top-header">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="wrap-top-information">
          <ul class="list-inline left">
            <li><a href="#"><i class="fa fa-phone"></i><span>9810521407</span></a></li>
            <li><a href="#"><i class="fa fa-question-circle"></i><span>Help </span></a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="wrap-top-information">
          <ul class="list-inline right">
            @if( ! auth()->check() )
                <li><a href="#" id="signUp" data-toggle="modal" data-target="#myModal"><i class="fa "><img src="{{ asset('public/images/flag.png') }}" alt="India"></i><span>Sign Up </span></a></li>
                <li><a href="#" id="signin" data-toggle="modal" data-target="#myModal">Sign In</a></li>
            @else
                <li>Welcome 
                    <a href="{{ url('/myaccount') }}">{{ auth()->user()->name }}</a> | 
                    <a href="{{ url('/signout') }}">Signout</a>
                </li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--====  End of Section pageheader  ====-->
<nav class="navbar navbar-default navbar-sticky white bootsnav"> 
  <!-- Start Top Search -->
  <div class="top-search">
    <div class="container">
      <div class="input-group"> <span class="input-group-addon"> <i class="fa fa-search"> </i> </span>
        <input class="form-control" placeholder="Search" type="text">
        <span class="input-group-addon close-search"> <i class="fa fa-times"> </i> </span> </div>
    </div>
  </div>
  <!-- End Top Search -->
  <div class="container"> 
    <!-- Start Atribute Navigation -->
    <div class="attr-nav">
      <div class="social hidden-xs hidden-sm">
        <ul>
          <li class="search"> <a href="#"> <i class="fa fa-search"> </i> </a> </li>
        </ul>
      </div>
    </div>
    <!-- End Atribute Navigation --> 
    <!-- Start Header Navigation -->
    <div class="navbar-header">
      <button class="navbar-toggle" data-target="#navbar-menu" data-toggle="collapse" type="button"> <i class="fa fa-bars"> </i> </button>
      <a class="navbar-brand" href="{{ url('/') }}"> <img src="{{ asset('public/images/clemira-property-black.png') }}" class="logo" alt=""> </a> </div>
    <!-- End Header Navigation --> 
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-menu">
      <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
        <li><a class="dropdown-toggle" data-toggle="dropdown" href="{{ url('/') }}">Home</a></li>
        <li><a href="{{ url('/page/about-us') }}/">About Us</a></li>
        <li class="dropdown megamenu-fw">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">State-Wise Rera</a>
            <ul class="dropdown-menu megamenu-content" role="menu">
                <li><div id="allStates" class="row"></div></li>
            </ul>
        </li>
        <li><a href="#">RERA For All</a></li>
        <li><a href="#">Rera Registered</a></li>
        <li><a href="{{ url('/blogs') }}/">Blogs</a></li>
        <li><a href="{{ url('/contact') }}/">Contact us</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!-- End Navigation -->