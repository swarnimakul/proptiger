<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<base href="{{ url()->to('/') }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Welcome To Mera RERA</title>

	<!-- start css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/fontawesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/animate.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootsnav.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/chosen.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/responsive.css') }}">

	<!-- Color -->
	<link rel="stylesheet" type="text/css" id="skin"  href="{{ asset('public/css/themes/default.css') }}">
	<!-- google font  -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i%7COpen+Sans:300,300i,400,400i,600,600i,700,800" >
</head>
<body>
<!-- Start preloading --> 
<!--<div class="spinner-wrapper">
  
  <p>Please wait ....</p>
</div>--> 
<!-- End preloading --> 

@include('layouts.header')

<div class="clearfix"> </div>

@yield('content')


</body>
</html>
