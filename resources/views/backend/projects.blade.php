@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/projects').'/' }}" class="current">Projects</a> </div>
    <h1>{{!empty($builderName)?$builderName:'Projects List'}}</h1>
  </div>
  @if(session('info'))
<div class="alert alert-success">{{session('info')}}</div>
  @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5><a href="{{ URL::to('backend/add-projects').'/' }}">Add projects</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                   <th>Builder Name</th>
                   <th>Location</th>
                  <th>Status</th>
                  
                  <th>Action</th>
                  
                </tr>
              </thead>
              <tbody>
                @if(count($projectList)>0)
              @foreach( $projectList as $project )
                      
  <tr class="gradeU">
    <td>{{$project->project_name}}</td>
                   @if($project->builder_name=='')
<td class="center">----</td>
                  @else
 <td>{{ $project->builder_name }}</td>
                  @endif

<td class="center">{{ ProjectsController::getProjLoc($project->location_id)}}</td>
 <td class="center">  @if($project->status == 'no')
                    <button class="btn btn-danger btn-mini" onclick="updateProjectStatus('{{ $project->status }}',{{ $project->id }})">Deavtive</button>
                    @endif
                     @if($project->status == 'yes')
                   
                    <button class="btn btn-success btn-mini" onclick="updateProjectStatus('{{ $project->status }}',{{ $project->id }})">Active</button>

                    @endif
                     </td>
                 
                   
  <td >
  <a href="{{url('/')}}" target="_blank" title="View"><i class="icon-eye-open" ></i></a> | 
  <a href="{{url('/')}}/backend/update-projects/{{$project->id}}" title="Edit"><i class="icon-edit"></i></a>
                  </td>
                 
                </tr>
              @endforeach
            @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<script type="text/javascript">
  function updateProjectStatus(is_active,u_id){
    var is_active = is_active;
    var u_id=u_id;
    var msg='';
     var url="{{url('/')}}/backend/projects/{uid}";
    if(is_active=='yes'){
    msg=' Deactive ';
  }
  else{
    msg=' Activate ';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {is_active:is_active,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
@include('backend.layouts.footer')


