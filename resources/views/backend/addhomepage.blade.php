@include('backend.layouts.master')

<!--Header-part to commit-->
<style type="text/css">
  .img{
    width: 200px;
    border: 1px solid green;
    border-radius: 5px;
  }

</style>
  @include('backend.layouts.header')
  <link rel="stylesheet" href="{{asset('public/backend/css/colorpicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/datepicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/pages').'/' }}" class="tip-bottom">Pages</a> <a href="" class="current">{{ ! empty($Page->title) ? $Page->title : 'Add New Home Page' }}</a> </div>
  <h1>{{ ! empty($Page->title) ? $Page->title : 'Add New Home Page' }}</h1>
</div>


<form  method="post" action="" enctype="multipart/form-data">
  {{ csrf_field() }}
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{ ! empty($Page->title) ? $Page->title : 'Add New Home Page' }}</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                  <input type="text"  class="span11" name="title" value="{{ ! empty($Page->title) ? $Page->title : '' }}" required>
                </div>
              </div>
            <div class="control-group">
                <label class="control-label">Url</label>
                <div class="controls">
                  <input type="text" class="span11" value="{{ ! empty($Page->url) ? $Page->url : '' }}" name="url" required>
                </div>
              </div>
            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                
                <textarea class="span11" name="meta_tag" value="" >{{ ! empty($Page->meta_tag) ? $Page->meta_tag : '' }}</textarea>
             
              </div>
            </div>
            <div class="control-group">
    <label class="control-label">Meta Description
:</label>
              <div class="controls">
               <textarea class="span11"  name="meta_desc" value=""  >{{ ! empty($Page->meta_desc) ? $Page->meta_desc : '' }}</textarea>
              </div>
            </div>
         <!-- og title -->
  <div class="control-group">
<label class="control-label">OG Title:</label>

              <div class="controls">
               <textarea class="span11" name="  og_title" value=""  >{{ ! empty($Page-> og_title) ? $Page-> og_title : '' }}</textarea>
              </div>
            </div>
         <!-- og title close --> 
         <!-- og description -->
  <div class="control-group">
<label class="control-label">OG Description:</label>

              <div class="controls">
               <textarea class="span11" name="og_description" value=""  >{{ ! empty($Page->   og_description) ? $Page-> og_description : '' }}</textarea>
              </div>
            </div>
         <!-- close og description -->
         <!-- og url -->

  <div class="control-group">
<label class="control-label">OG Url:</label>

              <div class="controls">
               <textarea class="span11" name="og_url" value=""  >{{ ! empty($Page-> og_url)?$Page->og_url : '' }}</textarea>
              </div>
            </div>

         <!-- og url close -->

<!-- og image -->
<div class="control-group">
                <label class="control-label"> OG Url</label>
                <div class="controls">
                   <input type="file" id="image" value="{{!empty($edit)?$edit->og_image:''}}" name="og_image"  onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview2" src="{{!empty($edit->og_image)?'../../public/upload/builder_logo/'.$edit->og_image:''}}"
    >
                </div>
              </div>

<!-- og image close -->
<!-- banner image upload -->
  <div class="control-group">
                <label class="control-label"> Banner Image</label>
                <div class="controls">
                   <input type="file" id="image" value="{{!empty($edit)?$edit->banner_image:''}}" name="  banner_image"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src="{{!empty($edit->logo)?'../../public/upload/builder_logo/'.$edit->logo:''}}"
    >
                </div>
              </div>
<!-- close banner image -->            
          
          </div>
        </div>
      </div>

 
    
 
  </div>
  <!-- content  -->
<div class="widget-box">
      <div class="widget-title"> 
        <h5>content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
<textarea class="span12" rows="6" placeholder="Enter text ..." id="textarea1" name="content"  ></textarea>
            </div>
         
        </div>
      </div>
    </div>
  <!-- close content -->
  <div class="row-fluid">
     <div class="control-group">
              <label>
                  <div class="checker" ><span>
                    <div class="checker" ><span>
                      
                     
           <input type="checkbox" name="is_active" style="opacity: 0;"   >
                    </span></div>
                </span>
                </div>
                  Active/Deactive</label>
                <label>
             </label></div>
              </div>
               <div class="row-fluid">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Right Module</h5>
      </div>
      <div class="widget-content">
         <div class="control-group">
              
              <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea2" name="right_module" value="" required></textarea>
      
          
        </div>
      </div>
    </div>
  </div>
  <!-- left module -->
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Left Module</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="left_module" value="" ></textarea>
            </div>
         
        </div>
      </div>
    </div>
 <!-- close left module -->
<!-- middle content -->
<div class="widget-box">
      <div class="widget-title"> 
        <h5>Middle Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea3" name="  middle_content" value="" >{{ ! empty($Page->  middle_content) ? $Page-> middle_content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
<!-- close middle content -->
<!-- Our Partners -->
<div class="widget-box">
      <div class="widget-title"> 
        <h5>  Our Partner</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea4" name="   our_partner" value="" >{{ ! empty($Page->   our_partner) ? $Page->our_partner : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
<!-- close our partners -->

<!-- location features -->
<div class="widget-box">
      <div class="widget-title"> 
        <h5>Location Features </h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
<textarea class=" span12" rows="6" placeholder="Enter text ..." id="text" name="location_features" >{{ ! empty($Page->location_features ) ? $Page-> location_features: '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>

<!-- close features -->
    <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
</div>




</form>
</div></div></div>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');
CKEDITOR.replace('textarea1');
CKEDITOR.replace('textarea2');
CKEDITOR.replace('textarea3');
CKEDITOR.replace('textarea4');
CKEDITOR.replace('text');
CKEDITOR.replace('textarea5');</script>
<<!--Footer-part-->
@include('backend.layouts.footer')
