@include('backend.layouts.master')
<style type="text/css">

#geocomplete { width: 200px}

   .map_canvas { 
       width: 800px; 
        height: 380px; 
        margin: 10px 20px 10px 0;

      }
     
</style>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
</style>
<!--Header-part-->


@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" >Profile</a> <a class="current">Edit Profile</a> </div>
  <h1>Update Profile  </h1>
</div>
 <div class="container-fluid">
  <hr>
 <form method="post" class="form-horizontal" enctype="multipart/form-data">
  {{ csrf_field() }}
@if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
  
 <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Profile</h5>
        </div>
         <div class="control-group">
              <label class="control-label">UserName :</label>
              <div class="controls">
                <input type="text" name="name" required="" value="{{!empty($user)?$user->name:''}}" class="span4" placeholder="" />
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input type="text" name="email" required="" value="{{!empty($user)?$user->email:''}}" class="span4" placeholder="" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Phone Number :</label>
              <div class="controls">
                <input type="text" name="phone" required="" value="{{!empty($user)?$user->phone:''}}" class="span4" placeholder="" />
              </div>
            </div>

        <div class="widget-content " >
            <div class="control-group">
              <label class="control-label">Address</label>
                <div class="controls">
                      <input id="geocomplete" name="address" type="text" placeholder="Type in an address" value="{{!empty($user)?$user->address:''}}" size="90" /></div>
                
            </div>
            <div class="control-group" style="margin-left: 130px;">
              <div class="map_canvas"></div>
            </div>
            <div id="info" class="control-group">
              <div class="controls">
                      
                <input name="lat" type="hidden" value="{{!empty($user)?$user->latitude:''}}">
                       
                <input name="lng" type="hidden" value="{{!empty($user)?$user->longitude:''}}">
              </div>
           </div>
        </div>

        </div>
  </div>

  <!-- submit btn -->
            <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Update Profile</button>
            </div>

 </form>
</div>
</div>
</div>
<!--Footer-part-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--Footer-part-->
@include('backend.layouts.footer')
