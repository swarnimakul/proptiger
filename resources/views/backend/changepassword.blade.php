@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
  .sp{
    color:red;
    margin-left:10px;
    text-transform: uppercase;
  }
  .sps{
    color:green;
    margin-left:10px;
    text-transform: uppercase;
  }
</style>
<!--Header-part-->


@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" >Profile</a> <a class="current">Change Password</a> </div>
  <h1>Change Password </h1>
</div>
 <div class="container-fluid">
  <hr>
 <form method="post" class="form-horizontal" enctype="multipart/form-data">
  {{ csrf_field() }}
@if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
  
 <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Change Password</h5>
        </div>
             <div class="control-group">
              <label class="control-label">Current Password:</label>
              <div class="controls">
                <input type="text" name="cupassword" required="" value="" class="span4" placeholder="" />
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">New Password:</label>
              <div class="controls">
                <input type="text" id="password-1" name="password" required="" value="" class="span4" placeholder="" />
              </div>
            </div>

            <div class="control-group">
          <label class="control-label">Confirm Password:</label>
              <div class="controls">
                <input type="text" id="password-2" name="cpassword" required=""  value="" class="span4 password" placeholder="" onchange="change();" />
               
              </div>
            </div>

       

        </div>
  </div>

  <!-- submit btn -->
            <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Change Password</button>
            </div>

 </form>
</div>
</div>
</div>
<!--Footer-part-->
<script type="text/javascript">
  function change(){
      if($("#password-1").val() == $("#password-2").val()){
            /* they match */
            $('.sp').remove('span');
            $("#password-2").after('<span class="sps"><i class="icon-ok-sign"></i></span>');
      }else{
            /* they are different */
             $('.sps').remove('span');
              $("#password-2").after('<span class="sp"><i class="icon-remove-sign"></i></span>');
      }
 }

</script>
<!--Footer-part-->
@include('backend.layouts.footer')
