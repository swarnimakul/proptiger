
@include('backend.layouts.master')
<style type="text/css">
  .img{
    width: 100px;
    border: 1px solid green;
    border-radius: 5px;
  }

</style>
<script type="text/javascript">
  /* function addCat(){
    var newCat = $('#category').val();
    //alert (newCat);
     if(newCat == 'other_cat'){
      $("#othercatDiv").show();
        $("#otherCat").attr("placeholder", "Add new category").focus();
     }
     else{
      $("#othercatDiv").hide();
      
     }
    }*/

  function addLocation(){
    var newCat = $('#location').val();
    //alert (newCat);
     if(newCat == 'other_loc'){
      $("#otherlocDiv").show();
        $("#otherLoc").attr("placeholder", "Add new Location").focus();
     }
     else{
      $("#otherlocDiv").hide();
      
     }
    }
</script>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />

 
<!--Header-part-->

  @include('backend.layouts.header')
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/builders').'/' }}" >Builders</a> <a href="{{ URL::to('backend/add-builders').'/' }}" class="current"> {{!empty($edit)?'Update':'Add'}} builder </a> </div>
  <h1>{{!empty($edit)?'Update':'Add New '}} Builder</h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>{{!empty($edit)?'Update':'Add'}} </h5>
          </div>
          @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="#"  enctype="multipart/form-data"  >
                {{ csrf_field() }}

                @include('backend.layouts.errors')
               
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input type="text" name="name" required="" value="{{!empty($edit)?$edit->name:''}}" >
                </div>
              </div>
          
             <div class="control-group">
                 <label class="control-label" >Location</label>
              <div class="controls">
                <select name="location[]" multiple id="location" style="width: 220px;" onchange="addLocation()" >
                   @if(!empty($locations))
                  @foreach($locations as $loc)
			  
                  @if(!empty($edit)&&!empty($locarray))
					  @foreach($locarray as $array)
				  @if($array->location==$loc->location)
                  <option value="{{$array->id}}" selected>{{$loc->location}}</option>
			  @endif
			  @endforeach
                 @endif
                     @if(!empty($edit)&& $edit->location!=$loc->location)
                      <option value="{{$loc->id}} ">{{$loc->location}}</option>
                 @endif
                   @if(empty($edit))
                    <option value="{{$loc->id}} ">{{$loc->location}}</option>
                  @endif
                  @endforeach
                  @endif
                  <option value="other_loc">Other</option>
                 
                </select>
              </div>
              </div>

               <div class="control-group" id="otherlocDiv" style="display: none;" >
                 <label class="control-label"></label>
                <div class="controls">
                      <input type="text" name="otherLoc" id="otherLoc"  >
               
                </div>
              </div>
              <div class="control-group">
                <label class="control-label"> Builder Logo</label>
                <div class="controls">
                   <input type="file" id="image" value="{{!empty($edit)?$edit->logo:''}}" name="logo"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src="{{!empty($edit->logo)?'../../public/upload/builder_logo/'.$edit->logo:''}}"
    >
                </div>
              </div>
              <div class="widget-box">
      <div class="widget-title"> 
        <h5>Description</h5>
      </div>
      <div class="widget-content">
               <div class="control-group"  >
                <div class="">
                     <textarea cols="3" id="textarea"  name="description" required="">{{!empty($edit)?$edit->description:''}}</textarea>
               
                </div>
              </div>
            </div>
          </div>
              <div class="form-actions">
              <button type="submit" class="btn btn-success">{{!empty($edit)?'Update':'Save'}}</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--Footer-part-->
@include('backend.layouts.footer')
