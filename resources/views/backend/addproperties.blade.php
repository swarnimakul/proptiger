@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
   #geocomplete { width: 200px}

   .map_canvas { 
       width: 800px; 
        height: 330px; 
        margin: 10px 20px 10px 0;
      }
       .remove-btn{
    font-size:18px;
    color: #c75c59;
 
  }
  .margin{
    margin-left: 400px;
  }
</style>
<!--Header-part-->
<script>
function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<img style='margin-left:5px;margin-top:5px;' class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"' width='80' height='80'>");
 }
}
</script>
<script type="text/javascript">
   function addCat(){
    var newCat = $('#category').val();
    //alert (newCat);
     if(newCat == 'other_cat'){
      $("#othercatDiv").show();
        $("#otherCat").attr("placeholder", "Add new category").focus();
     }
     else{
      $("#othercatDiv").hide();
      
     }
    }
    function addLocation(){
    var newCat = $('#location').val();
    //alert (newCat);
     if(newCat == 'other_loc'){
      $("#otherlocDiv").show();
        $("#otherLoc").attr("placeholder", "Add new Location").focus();
     }
     else{
      $("#otherlocDiv").hide();
      
     }
    }
  </script>
@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}" >Properties</a> <a href="{{ URL::to('backend/add-property').'/' }}" class="current"> Add Property</a> </div>
  <h1>Add New Property  </h1>
</div>
 <div class="container-fluid">
  <hr>
  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Basic info</h5>
        </div>
        <div class="widget-content nopadding">
              @include('backend.layouts.errors')
                
    
            <div class="control-group">
              <label class="control-label">Name </label>
              <div class="controls">
                <input type="text" class="span11" required="" name="name" placeholder="name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Url :</label>
              <div class="controls">
                <input type="text" name="url" required="" class="span11" placeholder="Url(ex:property-name)" />
              </div>
            </div>
            
            <!-- rera_id -->
             <div class="control-group">
              <label class="control-label">Rera Id :</label>
              <div class="controls">
                <input type="text" name="rera_id"  class="span11" placeholder="id" />
              </div>
            </div>
            <!-- close -->
            <div class="control-group">
              <label class="control-label">Select Catecory</label>
              <div class="controls">
               <select name="category" id="category" onchange="addCat()"  >
                  <option value=" ">-Select Category-</option>
                    @foreach($categories as $cat)
                   <option value="{{$cat->id}} ">{{$cat->category}} </option>
                   @endforeach
                 <option value="other_cat">Other</option>
                </select>
              </div>
            </div>
            <div class="control-group" id="othercatDiv" style="display: none;" >
                 <label class="control-label"></label>
                <div class="controls">
                      <input type="text" name="otherCat" id="otherCat"  >
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Location</label>
              <div class="controls">
              <select name="l_id[]" id="location" multiple="" onchange="addLocation()"  >
                  @foreach($locations as $loc)
                      <option value="{{$loc->id}}">{{$loc->location}}</option>
                   @endforeach
                   <option value="other_loc">Other</option>
                </select>
              </div>
            </div>
            <div class="control-group" id="otherlocDiv" style="display: none;" >
                 <label class="control-label"></label>
                <div class="controls">
                      <input type="text" name="otherLoc" id="otherLoc"  >
               
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Builder</label>
               <div class="controls">
                <select name="b_id">
                  <option value=" ">-Select Builder-</option>
                  @foreach($builders as $builder)
                      <option value="{{$builder->id}}">{{$builder->name}}</option>
                   @endforeach
                 </select>
              </div>
             </div>

            <div class="control-group">
              <label class="control-label">Project</label>
               <div class="controls">
                <select name="project_id">
                  <option value=" ">-Select Project-</option>
                  @foreach($projects as $project)
                  <option value="{{$project->id}}">{{$project->name}}</option>
                  @endforeach
                 </select>
              </div>
             </div>

        </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Other Info</h5>
        </div>
        <div class="widget-content nopadding">
             <div class="control-group">
              <label class="control-label">Website link</label>
              <div class="controls">
                <input type="text"  class="span11" name="website" />
              </div>
            </div>
           
            <div class="control-group">
              <label class="control-label">Sold</label>
              <div class="controls">
                <input type="text"  class="span11" name="sold" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Contract</label>
              <div class="controls">
                <input type="text" name="contract"  class="span11" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Offer</label>
              <div class="controls">
                <input type="text" name="offer"  class="span11" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Possession</label>
              <div class="controls">
                <input type="text" name="Possession"  class="span11" />
              </div>
            </div>
            <!-- launch apartment  -->
            <div class="control-group">
              <label class="control-label">Total Launched apartments</label>
                <div class="controls">
                <input type="text" name="total_launched_apartments" class="span11">
                </div>
            </div>


            <!-- close  -->
            <!-- Launch Date  -->
            <div class="control-group">
              <label class="control-label">Launch Date</label>
                <div class="controls">
                <input type="Date" name="launch_date" class="span11">
                </div>
            </div>


            <!-- close  -->

             <!-- Availability -->
            <div class="control-group">
              <label class="control-label">Availability</label>
                <div class="controls">
                <input type="text" name="availability" class="span11">
                </div>
            </div>
            <!-- close  -->

            <!-- status -->
            <div class="control-group">
              <label class="control-label">Status</label>
                <div class="controls">
                <select name="status">
                  <option value=" ">-Select Status-</option>
                  <option value="Ready To Move">Ready</option>
                  <option value="Under Construction">under Construction</option>
                  <option value="Launch Soon">Launch Soon</option>
                  <option value="Hold">Hold</option>
                 
                 </select>
              </div>
            </div>
            <!-- close  -->
            <div class="control-group">
              <label class="control-label"> upload Broucher</label>
              <div class="controls">
                <input type="file" name="broucher" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label"> Upload Banner Image</label>
              <div class="controls">
                <input type="file" name="banner_image" />
              </div>
            </div>
           </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Attribute</h5>
        </div>
        <div class="widget-content nopadding">
            <div class="control-group">
              <label class="control-label">Rooms</label>
              <div class="controls">
                <input type="text" name="room"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bed</label>
              <div class="controls">
                <input type="text" name="bed"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bath</label>
              <div class="controls">
                <input type="text" name="bath"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Home Area</label>
              <div class="controls">
                <input type="text" name="home_area"  value="" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Lot area</label>
              <div class="controls">
                <input type="text" name="lot_area"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Lot Dimensions</label>
              <div class="controls">
                <input type="text" name="lot_dimension"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Size (sq. ft.)</label>
              <div class="controls">
                <input type="text" name="size_sq_ft"  value="" class=" span11">
                 
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Size (sq. m.)</label>
              <div class="controls">
                <input type="text" name="size_sq_m"  value="" class=" span11">
                 
              </div>
            </div>
 			<div class="control-group">
              <label class="control-label">Garages</label>
              <div class="controls">
                <input type="text" name="garages"  value="" class=" span11">
                
              </div>
            </div>
<!-- featured open -->
            <div class="control-group">
              <label class="control-label">Featured</label>
                <div class="controls">
                <input type="checkbox" name="is_featured" class="span11">
                </div>
            </div>
<!-- featured close -->

         </div>
      </div>
<div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Loan Details</h5>
        </div>
        <div class="widget-content nopadding">
           
            <div class="control-group">
              <label class="control-label">Loan Available</label>
                <div class="controls">
                <input type="checkbox" name="loan_available" id="check" value="yes" class="span11">
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Loan Amount(min)</label>
              <div class="controls">
                <input type="text" id="loan_amount" name="loan_amount"  value="" class=" span11" disabled>     
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Monthly EMI</label>
              <div class="controls">
            <input type="text" id="monthly_emi" name="monthly_emi"  value="" class=" span11" disabled>
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">EMI starts at</label>
              <div class="controls">
                <input type="text" id="emi_starts_at" name="emi_starts_at"  value="" class=" span11" disabled>
                
              </div>
            </div>
         </div>
      </div>


    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Amenities</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 300px;">
              <div class="controls">
                @foreach($amenities as $amenity)
                <label>
                  <input type="checkbox" value="{{$amenity->id}}" name="amenity[]" />
                  {{$amenity->name}}
                </label>
                
                @endforeach
              </div>
            </div>
         </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Type</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 258px;">
               <div class="controls">
                @foreach($types as $type)
                <label>
                  <input type="checkbox" value="{{$type->id}}" name="type[]" />
                  {{$type->name}}
                </label>
                
                @endforeach
              </div>
            </div>
        </div>
    </div>
     <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Specifications</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 258px;"> 
    		@foreach($specifications as $specification)
            <div class="controls">
                <label><b>{{$specification->specification}}</b></label>
               @php $features=PropertiesController::getfeatures($specification->id) @endphp
               @foreach($features as $feature)
                <label>
                 <span style="color: green"> <b>{{$feature->specification}} : </b></span>
                </label>
               	@php $featureArr = explode(",",$feature->feature) ;
                     //print_r($featureArr);
                     //die;
               	@endphp
               		@foreach($featureArr as $val)
               		 <label>
                  		<input type="checkbox" value="{{$specification->id}}_{{$feature->id}}_{{$val}}" name="feature[]" />
                  		{{$val}}
                	</label>
                	@endforeach
               
                @endforeach
		    </div> 
            @endforeach
            </div>
        </div>
    </div>
     <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Gallery</h5>
        </div>
        <div class="widget-content nopadding" id="addmorediv1">
            <div class="control-group">
              <label class="control-label"> Upload Images</label>
                <div class="controls">
                    <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
                </div>
               
             </div>

              <div class="control-group" id="image_preview"></div>
           <br>
            </div>
        </div>
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Floor Plan</h5>
        </div>
        <div class="widget-content nopadding" >
            <div class="control-group">
              <label class="control-label">Type</label>
                <div class="controls">
                    <input name="floor_plan_type[]" class="form-control" />
                </div>   
             </div>
             <div class="control-group">
              <label class="control-label">Image</label>
                <div class="controls">
                    <input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/>
                </div>   
             </div>
            </div>
            <div id="add"></div>
          <div class="control-group">
            <label class="control-label"></label>
            <a href="#" id="add-more" style="margin-left: 200px;"><i  class="icon-plus-sign add-btn"></i>ADD MORE</a>
          </div>
        </div>
        
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Payment Details</h5>
        </div>
        <div class="widget-content nopadding">
        	 <div class="control-group">
              <label class="control-label">Max Price</label>
              <div class="controls">
                <input type="text"  class="span11" name="max_price" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Min Price</label>
              <div class="controls">
                <input type="text"  class="span11" name="min_price" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Total Amount</label>
              <div class="controls">
                <input type="text"  class="span11" name="price" />
              </div>
            </div>
        	<div class="control-group">
              <label class="control-label">Resale Price</label>
              <div class="controls">
                <input type="text" name="resale_price"  value="" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Builder Price</label>
              <div class="controls">
                <input type="text" name="builder_price"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Starting Price</label>
              <div class="controls">
                <input type="text" name="starting_price"  value="" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Booking amount(Pay after registation)</label>
              <div class="controls">
                <input type="text" name="booking_amount"  value="" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Booking payment time(days/months)</label>
              <div class="controls">
                <input type="text" name="booking_time" placeholder="Winthin days/months to pay after Registration"  value="" class=" span11">
             </div>
            </div>

          
         </div>
      </div>
    </div>

  </div>
  <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Address</h5>
        </div>
        <div class="widget-content" >
           <div class="control-group">
              <label class="control-label">Address</label>
                <div class="controls">
                      <input id="geocomplete" name="address" type="text" placeholder="Type in an address" size="90" /></div>
            <div class="control-group" style="margin-left: 130px;">
               <div class="map_canvas"></div>
            </div>
          <div id="info" class="control-group">
            <div class="controls">
                      Latitude
                <input name="lat" type="text" value="">
                      Longitude 
                <input name="lng" type="text" value="">
              </div>
           </div>
        </div>
        </div>
        </div>
  </div>
  <div class="row-fluid">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Description</h5>
      </div>
      <div class="widget-content">
         <div class="control-group">
              
              <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="description" value="" required></textarea>
      
          
        </div>
      </div>
    </div>
  </div>
</div> 
         <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Save Property</button>
            </div>
</form>
</div>
</div>
</div>
<!--Footer-part-->

        
<script type="text/javascript">
 $(document).ready(function() {
  var i = 1;
$("#add-more").click(function(e){
  i++;
e.preventDefault();
var add='<div><hr><label class="control-label"></label><a href="#" id="remove" class="margin" title="REMOVE"><i class="icon-minus-sign remove-btn"></i> </a><div class="control-group"><label class="control-label">Type</label><div class="controls"><input name="floor_plan_type[]" class="form-control" /></div></div><div class="control-group"><label class="control-label">Image</label><div class="controls"><input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/></div></div>';
$("#add").append(add); 
        
    });
$(document).on('click','#remove',function(){
  $(this).closest('div').empty();
});
});
    
  /* $(document).ready(function() {
     
        
   $('select[name="l_id"]').on('change', function() {
            var locID = $(this).val();
            if(locID) {
                $.ajax({
                    url: "../ajaxLocBuilder/"+locID,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="b_id"]').empty();
                        $.each(data, function(key, value) {
                          //alert(value['name'])
                           $('select[name="b_id"]').append('<option value="'+ value['id'] +'">'+ value['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="b_id"]').append('<option value=" ">-Select Builder-</option>');
            }
        });
        $('select[name="l_id"]').on('change', function() {
            var locIDs = $(this).val();
            if(locIDs) {
                $.ajax({
                    url: "../ajaxProject/"+locIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                         // alert(values[0]['name'])
                           $('select[name="project_id"]').append('<option value="'+ values[0]['id'] +'">'+ values[0]['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });
 $('select[name="b_id"]').on('change', function() {
            var bIDs = $(this).val();
            if(bIDs) {
                $.ajax({
                    url: "../ajaxBuilderProject/"+bIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                          //alert(value['name'])
                           $('select[name="project_id"]').append('<option value="'+ values['id'] +'">'+ values['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });


    });
    
   */
</script>

<script>
  //loan available
$(function(){
$("#check").change(function(){
var str=!this.checked;
if(str){
$("#loan_amount").prop('disabled',true);
$("#monthly_emi").prop('disabled',true);
$("#emi_starts_at").prop('disabled',true);
}
else{
$("#loan_amount").prop('disabled',false);
$("#monthly_emi").prop('disabled',false);
$("#emi_starts_at").prop('disabled',false);
}
});
});

</script>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>

<!--Footer-part-->
@include('backend.layouts.footer')
