@include('backend.layouts.master')

<!--Header-part to commit-->

  @include('backend.layouts.header')
  <link rel="stylesheet" href="{{asset('public/backend/css/colorpicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/datepicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/pages').'/' }}" class="tip-bottom">Pages</a> <a href="" class="current">{{ ! empty($Page->title) ? $Page->title : 'Add New Page' }}</a> </div>
  <h1>{{ ! empty($Page->title) ? $Page->title : 'Add New Page' }}</h1>
</div>


<form  method="post" action="" >
  {{ csrf_field() }}
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{ ! empty($Page->title) ? $Page->title : 'Add New Page' }}</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                  <input type="text" class="span11" name="title" value="{{ ! empty($Page->title) ? $Page->title : '' }}" required>
                </div>
              </div>
            <div class="control-group">
                <label class="control-label">Url</label>
                <div class="controls">
                  <input type="text" class="span11" value="{{ ! empty($Page->url) ? $Page->url : '' }}" name="url" required>
                </div>
              </div>
            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                
                <textarea class="span11" name="meta_tag" value="" >{{ ! empty($Page->meta_tag) ? $Page->meta_tag : '' }}</textarea>
             
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Meta Description
:</label>
              <div class="controls">
               <textarea class="span11" name="meta_desc" value=""  >{{ ! empty($Page->meta_desc) ? $Page->meta_desc : '' }}</textarea>
              </div>
            </div>
          
            
          
          </div>
        </div>
      </div>

 
    
 
  </div>
  <div class="row-fluid">
     <div class="control-group">
              <label>
                  <div class="checker" ><span>
                    <div class="checker" ><span>
                      
                     
           <input type="checkbox" name="is_active" style="opacity: 0;"   {{! empty($Page->is_active) ? (($Page->is_active=='yes')?'checked':'') : '' }}>
                    </span></div>
                </span>
                </div>
                  Active/Deactive</label>
                <label>
             </label></div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="content" value="" >{{ ! empty($Page->content) ? $Page->content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
  </div>
    <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
</div>




</form>
</div></div></div>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
<<!--Footer-part-->
@include('backend.layouts.footer')
