@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> 
      <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
      <a href="{{ URL::to('backend/properties').'/' }}">Property</a>
      <a href="{{ URL::to('backend/specifications').'/' }}" class="current">Specifications</a>
     </div>
    <h1>Specifications</h1>
  </div>
  @if(session('info'))
<div class="alert alert-success">{{session('info')}}</div>
  @endif
  @if(!empty($message))
<div class="alert alert-success">{{$message}}</div>
@endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>
              @if(!empty($id))
              <a href="{{ URL::to('backend/add-feature').'/' .$id}}">Add Feature</a></h5>
              @else
              <a href="{{ URL::to('backend/add-specifications').'/' }}">Add Specification</a></h5>
              @endif
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  @if(!empty($id))
                   <th>Feature</th>
                   @endif
                  <th>Action</th>
                   
               </tr>
              </thead>
              <tbody>
              @if(count($specifications)>0)
              @foreach( $specifications as $specification )
              <tr class="gradeU">
                <td>{{$specification->specification}}</td>   
                  @if(!empty($id))
                   <td>{{$specification->feature}}</td> 
                  @endif        
                <td >
                  @if(empty($id))
                    <a href="{{url('/')}}/backend/update-specification/{{$specification->id}}" title="Edit"><i class="icon-edit"></i></a>
                    @endif
                     @if(!empty($id))
                     <a href="{{url('/')}}/backend/update-feature/{{$specification->id}}" title="Edit"><i class="icon-edit"></i></a>
                     @endif
                    @if(empty($id))
                    | <a href="{{url('/')}}/backend/add-feature/{{$specification->id}}" title="Add Features">Add features</a>|
                    <a href="{{url('/')}}/backend/view-feature/{{$specification->id}}" title="View Features">View features</a>
                    @endif
                </td>
              </tr>
              @endforeach
            @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->

@include('backend.layouts.footer')


