@include('backend.layouts.master')

<style type="text/css">
  .img{
    width: 50px;
    border: 1px solid green;
    border-radius: 5px;
  }
   .remove-btn{
    font-size:18px;
    color: #c75c59;
 
  }
#geocomplete { width: 200px}

   .map_canvas { 
       width: 800px; 
        height: 380px; 
        margin: 10px 20px 10px 0;

      }
      .margin{
    margin-left: 400px;
  }
</style>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
</style>
<!--Header-part-->
<script>
function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<img style='margin-left:5px;margin-top:5px;' class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"' width='80' height='80'>");
 }
}
</script>
<script type="text/javascript">
   function addCat(){
    var newCat = $('#category').val();
    //alert (newCat);
     if(newCat == 'other_cat'){
      $("#othercatDiv").show();
        $("#otherCat").attr("placeholder", "Add new category").focus();
     }
     else{
      $("#othercatDiv").hide();
      
     }
    }
    function addLocation(){
    var newCat = $('#location').val();
    //alert (newCat);
     if(newCat == 'other_loc'){
      $("#otherlocDiv").show();
        $("#otherLoc").attr("placeholder", "Add new Location").focus();
     }
     else{
      $("#otherlocDiv").hide();
      
     }
    }
  </script>
@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}" >Properties</a> <a href="" class="current">Update Property</a> </div>
  <h1>Update Property  </h1>
</div>
 <div class="container-fluid">
  <hr>
  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Basic info</h5>
        </div>
        <div class="widget-content nopadding">
              @include('backend.layouts.errors')
           <div class="control-group">
              <label class="control-label">Name </label>
              <div class="controls">
                <input type="text" class="span11" required="" name="name" value="{{!empty($propertyList)?$propertyList->name:''}}" placeholder="name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Url :</label>
              <div class="controls">
                <input type="text" name="url" required="" value="{{!empty($propertyList)?$propertyList->url:''}}" class="span11" placeholder="Url(ex:property-name)" />
              </div>
            </div>
              <!-- rera_id -->
             <div class="control-group">
              <label class="control-label">Rera Id :</label>
              <div class="controls">
                <input type="text" name="rera_id" value="{{!empty($propertyList)?$propertyList->rera_id:''}}" class="span11" placeholder="id" />
              </div>
            </div>
            <!-- close -->
            <div class="control-group">
              <label class="control-label">Select Catecory</label>
              <div class="controls">
               <select name="category" id="category" onchange="addCat()"  >
                  <option value=" ">-Select Category-</option>
                  @foreach($categories as $cat)
                   <option value="{{$cat->id}}" {{$cat->id == $propertyList->category_id? 'selected':''}} >{{$cat->category}} </option>
                   @endforeach
                  <option value=""> </option>
                 <option value="other_cat">Other</option>
                </select>
              </div>
            </div>
            <div class="control-group" id="othercatDiv" style="display: none;" >
    <label class="control-label"></label>
                <div class="controls">
     <input type="text" name="otherCat" id="otherCat"  >
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Location</label>
              <div class="controls">
              <select name="l_id[]" id="location" multiple="" onchange="addLocation()"  >
               <option value=" ">-Select Location-</option>
                  @foreach($locations as $loc)
                      <option value="{{$loc->id}}" {{in_array($loc->id ,$locIdArray)? 'selected':''}} >{{$loc->location}}</option>
                   @endforeach
                      <option value=""></option>
               
                   <option value="other_loc">Other</option>
                </select>
              </div>
            </div>
            <div class="control-group" id="otherlocDiv" style="display: none;" >
                 <label class="control-label"></label>
                <div class="controls">
                      <input type="text" name="otherLoc" id="otherLoc"  >
               
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Builder</label>
               <div class="controls" id="builderDropDown">
                <select name="b_id">
                  <option value=" ">-Select Builder-</option>
                  @foreach($builders as $builder)
                      <option value="{{$builder->id}}" {{$builder->id == $propertyList->builder_id? 'selected':''}} >{{$builder->name}}</option>
                   @endforeach  </select>
              </div>
             </div>

            <div class="control-group">
              <label class="control-label">Project</label>
               <div class="controls">
                <select name="project_id">
                  <option value=" ">-Select Project-</option>
                  @foreach($projects as $project)
                      <option value="{{$project->id}}" {{$project->id == $propertyList->project_id? 'selected':''}} >{{$project->name}}</option>
                   @endforeach
                 </select>
              </div>
             </div>

        </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Other Info</h5>
        </div>
        <div class="widget-content nopadding">
             <div class="control-group">
              <label class="control-label">Website link</label>
              <div class="controls">
                <input type="text"  value="{{!empty($propertyList)?$propertyList->web_url:''}}" class="span11" name="website" />
              </div>
            </div>
            <!-- max price -->
            <div class="control-group">
              <label class="control-label">Max Price</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->max_price:''}}" class="span11" name="max_price" />
              </div>
            </div>
            <!-- close max price -->
            <!-- min price -->
            <div class="control-group">
              <label class="control-label">Min Price</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->min_price:''}}"  class="span11" name="min_price" />
              </div>
            </div>
            <!-- close min price -->
            <div class="control-group">
              <label class="control-label">Price</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->price:''}}" class="span11" name="price" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Sold</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->sold:''}}" class="span11" name="sold" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Contract</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->contract:''}}" name="contract"  class="span11" />
              </div>
            </div>
            <!-- offer -->
           <div class="control-group">
              <label class="control-label">Offer</label>
              <div class="controls">
                <input type="text"  value="{{!empty($propertyList)?$propertyList->offer:''}}"   name="offer"  class="span11" />
              </div>
            </div>
            <!-- close offer -->
            <!-- possession -->
            <div class="control-group">
              <label class="control-label">Possession</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->possession:''}}" name="Possession"  class="span11" />
              </div>
            </div>
            <!-- close possession -->
                 <!-- launch apartment  -->
            <div class="control-group">
              <label class="control-label">Total Launched apartments</label>
                <div class="controls">
                <input type="text" name="total_launched_apartments" value="" class="span11">
                </div>
            </div>


            <!-- close  -->
            <!-- Launch Date  -->
            <div class="control-group">
              <label class="control-label">Launch Date</label>
                <div class="controls">
                <input type="Date" value="{{!empty($propertyList)?$propertyList->launch_date:''}}" name="launch_date" class="span11">
                </div>
            </div>


            <!-- close  -->

             <!-- Availability -->
            <div class="control-group">
              <label class="control-label">Availability</label>
                <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->availability:''}}" name="availability" class="span11">
                </div>
            </div>
            <!-- close  -->

            <!-- status -->
            <div class="control-group">
              <label class="control-label">Status</label>
               <div class="controls">
                <select name="status">
                  <option value=" ">-Select Status-</option>
                  
                       <option value="Ready To Move" {{!empty($propertyList->status)?($propertyList->status=='Ready')?'selected':'':''}}>Ready</option>
                   <option value="under Construction" {{!empty($propertyList->status)?($propertyList->status=='under Construction')?'selected':'':''}}>under Construction</option>
                  <option value="Launch Soon" {{!empty($propertyList->status)?($propertyList->status=='Launch Soon')?'selected':'':''}}>Launch Soon</option>
                  <option value="Hold" {{!empty($propertyList->status)?($propertyList->status=='Hold')?'selected':'':''}}>Hold</option>
                  
                 </select>
              </div>
            </div>
            <!-- close  -->
            <!-- banner image -->
           <div class="control-group">
              <label class="control-label"> Upload Banner Image</label>
              <div class="controls">
                <input type="file" name="banner_image" onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])"/>
            <img class="img" id="preview2" src="{{!empty($propertyList)?'../../public/upload/property_banner_image/'.$propertyList->banner_image:''}}">
            
              </div>
            </div>
            <!-- close -->
                <!-- file upload -->
           <div class="control-group">
              <label class="control-label"> upload Brochuer</label>
              <div class="controls">
                <input type="file" id="image" accept="application/pdf" value="" name="broucher" onchange="document.getElementById('preview').src = '../../public/upload/brouchers/pdf-icon.png">
                @if(!empty($propertyList->brochure))
                <a  href="../../public/upload/brouchers/{{$propertyList->brochure}}" target="_blank">{{$propertyList->brochure}}</a>
                 @endif
              </div>
            </div>
              <!-- close upload -->
           </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Attribute</h5>
        </div>
        <div class="widget-content nopadding">
            <div class="control-group">
              <label class="control-label">Rooms</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->rooms:''}}" name="room"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bed</label>
              <div class="controls">
                <input type="text" name="bed" value="{{!empty($propertyList)?$propertyList->bed:''}}" value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bath</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->bath:''}}" name="bath"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Home Area</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->home_area:''}}" name="home_area"  value="" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Lot area</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->lot_area:''}}" name="lot_area"   class=" span11">
                
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Lot Dimensions</label>
              <div class="controls">
                <input type="text" name="lot_dimension"  value="{{!empty($propertyList)?$propertyList->lot_dimension:''}}"  class=" span11">
                
              </div>
            </div>
       <!--size   -->
      <div class="control-group">
              <label class="control-label">Size (sq. ft.)</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->size_sq_ft:''}}" name="size_sq_ft"   class=" span11">
                 
              </div>
            </div>
         <!-- close -->
         <!-- size sq. m. -->
          <div class="control-group">
              <label class="control-label">Size (sq. m.)</label>
              <div class="controls">
                <input type="text" name="size_sq_m"  value="{{!empty($propertyList)?$propertyList->size_sq_m:''}}" class=" span11">
                 
              </div>
            </div>
<!-- close -->
            <div class="control-group">
              <label class="control-label">Garages</label>
              <div class="controls">
                <input type="text" name="garages"  value="{{!empty($propertyList)?$propertyList->garages:''}}"  class=" span11">
                
              </div>
            </div>
            <!-- featured open -->
            <div class="control-group">
              <label class="control-label">Featured</label>
                <div class="controls">
                    <input type="checkbox" name="is_featured"{{!empty($propertyList->is_featured) ? (($propertyList->is_featured=='yes')?'checked':'') : '' }}>
                
                </div>
            </div>
<!-- featured close -->
         </div>
      </div>
      <!-- loan available -->
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Loan Details</h5>
        </div>
        <div class="widget-content nopadding">
           
            <div class="control-group">
              <label class="control-label">Loan Available</label>
                <div class="controls">
                <input type="checkbox" name="loan_available" id="check" value="yes"  class="span11" {{($propertyList->loan_available=='yes')?'checked':''}}>
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Loan Amount(min)</label>
              <div class="controls">
                <input type="text" id="loan_amount" name="loan_amount"  value="{{!empty($propertyList)?$propertyList->loan_amount:''}}" class=" span11" disabled>     
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Monthly EMI</label>
              <div class="controls">
            <input type="text" id="monthly_emi" name="monthly_emi"  value="{{!empty($propertyList)?$propertyList->monthly_emi:''}}" class=" span11" disabled>
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">EMI starts at</label>
              <div class="controls">
                <input type="text" id="emi_starts_at" name="emi_starts_at"  value="{{!empty($propertyList)?$propertyList->emi_start_at:''}}" class=" span11" disabled>
                
              </div>
            </div>
         </div>
      </div>

    <!-- close -->
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Amenities</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group">
              <div class="controls" style="overflow-y: scroll;
    height: 280px;">
                @foreach($amenities as $amenity)
               
                @if(in_array($amenity->id,$typeIdArray))
               
                 <label>
                 <input type="checkbox" value="{{$amenity->id}}" name="amenity[]" checked/>
                  {{$amenity->name}}
                </label>
                @else
                  <label>
                 <input type="checkbox" value="{{$amenity->id}}" name="amenity[]" />
                  {{$amenity->name}}
                </label>
                @endif
                 
                @endforeach

               
              </div>
            </div>
         </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Type</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 258px;">
               <div class="controls">
                @foreach($types as $type)
               @if(in_array($type->id,$propertyTypeId))
                <label>
                  <input type="checkbox" value="{{$type->id}}" name="type[]" checked/>
                  {{$type->name}}
                </label>
                @else
                <label>
                  <input type="checkbox" value="{{$type->id}}" name="type[]" />
                  {{$type->name}}
                </label>
                 @endif
                @endforeach
             
              </div>
            </div>
        </div>
    </div>
    <!-- specifications -->
               <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Specifications</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 258px;"> 
        @foreach($specifications as $specification)
            <div class="controls">
                <label><b>
                  {{$specification->specification}}</b></label>
               @php $features=PropertiesController::getfeatures($specification->id) @endphp
               @foreach($features as $feature)
                <label>
                  <span style="color: green"> <b>{{$feature->specification}} : </b></span>
                </label>
                @php $featureArr = explode(",",$feature->feature) ;
                 
                @endphp

                  @foreach($featureArr as $val)
                   <label>
                  
                    @if(in_array($feature->id,$propertyfeatures))
                    <input type="checkbox" value="{{$specification->id}}_{{$feature->id}}_{{$val}}" name="feature[]" checked />
                    
                      {{$val}}
                  </label>
                @else

                <input type="checkbox" value="{{$specification->id}}_{{$feature->id}}_{{$val}}" name="feature[]" />
                   {{$val}}
                  </label>
                     @endif
                  @endforeach
               
                @endforeach
        </div> 
            @endforeach
            </div>
        </div>
    </div>
    <!-- close -->
    
     <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Gallery</h5>
        </div>
        <div class="widget-content nopadding" id="addmorediv1">
          <div class="control-group">
              <label class="control-label"> Upload Images</label>
                <div class="controls">
                    <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
                </div>
             </div>
            
              <div class="control-group" id="image_preview">
                 @if(!empty($gallery))
                    @foreach($gallery as $g_img)
                    <div style="width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px">
                    <img class="img-responsive" height="70" width="88"    style=" margin-left: 5px; margin-top: 5px; width: 88px; height: 70px; "src="{{'../../public/upload/property_gallery/'.$g_img->img_name}}"/>
                    <div><a href="javascript:void(0)" onclick="deleteImage({{$g_img->id}},{{$g_img->property_id}})">Delete</a></div>
                    </div>
                     @endforeach
                  @endif
              </div>
               <br>
           </div>    
    </div>

  <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Floor Plan</h5>
        </div>
        <div class="widget-content nopadding" id="addmorediv1">
            
            <div class="control-group">
              <label class="control-label">Type</label>
                <div class="controls">
                    <input name="floor_plan_type[]" class="form-control" />
                </div>   
             </div>
             <div class="control-group">
              <label class="control-label">Image</label>
                <div class="controls">
                    <input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/>
                </div>   
             </div>
            </div>
             <div id="add"></div>
          <div class="control-group">
            <label class="control-label"></label>
            <a href="#" id="add-more" style="margin-left: 200px;"><i  class="icon-plus-sign add-btn"></i>ADD MORE</a>
           <div class="control-group" id="image_f_preview" >
            
                 @if(!empty($floorPlan))
                    @foreach($floorPlan as $img)
                    <div style="width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px">
                    <img class="img-responsive" height="70" width="88"    style=" margin-left: 5px; margin-top: 5px; width: 88px; height: 70px; " src="{{'../../public/upload/floor_plans/'.$img->image}}"/>
                    <div><a href="javascript:void(0)" onclick="deletef_Image({{$img->id}},{{$img->property_id}})">Delete</a>  <span; style="margin-left: 10px;color: red">{{$img->type}}</span> </div> 
                    </div>

                     @endforeach

                  @endif
              </div>
              <br>
           </div>    
    </div>
   <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Payment Details</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group">
              <label class="control-label">Max Price</label>
              <div class="controls">
                <input type="text"  class="span11" name="max_price" value="{{!empty($propertyList->max_price)?$propertyList->max_price:''}}" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Min Price</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList->min_price)?$propertyList->min_price:''}}" class="span11" name="min_price" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Total Amount</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList->price)?$propertyList->price:''}}" class="span11" name="price" />
              </div>
            </div>
          <div class="control-group">
              <label class="control-label">Resale Price</label>
              <div class="controls">
                <input type="text"  name="resale_price"  value="{{!empty($propertyList->resale_price)?$propertyList->resale_price:''}}" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Builder Price</label>
              <div class="controls">
                <input type="text" name="builder_price"   value="{{!empty($propertyList->builder_price)?$propertyList->builder_price:''}}" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Starting Price</label>
              <div class="controls">
                <input type="text" name="starting_price"  value="" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Booking amount(Pay after registation)</label>
              <div class="controls">
                <input type="text" name="booking_amount"  value="" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Booking payment time(days/months)</label>
              <div class="controls">
                <input type="text" name="booking_time" placeholder="Winthin days/months to pay after Registration"  value="" class=" span11">
             </div>
            </div>

          
         </div>
      </div>   
  
  </div>
  <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Address</h5>
        </div>
        <div class="widget-content " >
            <div class="control-group">
              <label class="control-label">Address</label>
                <div class="controls">
                      <input id="geocomplete" name="address" type="text" placeholder="Type in an address" value="{{$propertyList->address}}" size="90" /></div>
                
            </div>
            <div class="control-group" style="margin-left: 130px;">
              <div class="map_canvas"></div>
            </div>
            <div id="info" class="control-group">
              <div class="controls">
                      Latitude
                <input name="lat" type="text" value="{{$propertyList->latitude}}">
                      Longitude 
                <input name="lng" type="text" value="{{$propertyList->longitude}}">
              </div>
           </div>
        </div>

        </div>
  </div>

  </div>
  <div class="row-fluid">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Description</h5>
      </div>
      <div class="widget-content">
         <div class="control-group">
              <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="description" value="" required>{{!empty($propertyList)?$propertyList->description:''}}</textarea>
         </div>
      </div>
    </div>
  </div>
</div> 
         <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Update Property</button>
            </div>
</form>
</div>
</div>
</div>
<!--Footer-part-->
<script type="text/javascript">
$(document).ready(function() {
  
  $("#add-more").click(function(e){

    e.preventDefault();
    var add='<div><hr><label class="control-label"></label><a href="#" id="remove" class="margin" title="REMOVE"><i class="icon-minus-sign remove-btn"></i> </a><div class="control-group"><label class="control-label">Type</label><div class="controls"><input name="floor_plan_type[]" class="form-control" /></div></div><div class="control-group"><label class="control-label">Image</label><div class="controls"><input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/></div></div>';
    $("#add").append(add); 
  });
    $(document).on('click','#remove',function(){
      $(this).closest('div').empty();
    });
});
  function deleteImage(imgId,propId){
     var imgId  = imgId;
     var Url = "{{url('/')}}/backend/deletePropImg/"+imgId+"/"+propId;
     if(confirm("Are you sure to delete image")){
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
         //alert(data);
          $('#image_preview').html(data);
        }
      });
    }
  }
  function deletef_Image(imgId,propId){
     var imgId  = imgId;
     var Url = "{{url('/')}}/backend/deletePropf_Img/"+imgId+"/"+propId;
     if(confirm("Are you sure to delete image")){
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
         //alert(data);
          $('#image_f_preview').html(data);
        }
      });
    }
  }
 /* $(document).ready(function() {
   $('select[name="l_id"]').on('change', function(){
            var locID = $(this).val();
              if(locID) {
               $.ajax({
                    url: "../ajaxBuilder/"+locID,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                     $('select[name="b_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="b_id"]').append('<option value="'+ value['id'] +'">'+ value['name'] +'</option>');
                        });
                        
                    }
                });
            }else{

                $('select[name="b_id"]').append('<option value=" ">-Select Builder-</option>');
            }
        });
        $('select[name="l_id"]').on('change', function() {
            var locIDs = $(this).val();
            if(locIDs) {
                $.ajax({
                    url: "../ajaxProject/"+locIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                          //alert(value['name'])
                           $('select[name="project_id"]').append('<option value="'+ values['id'] +'">'+ values['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });
 $('select[name="b_id"]').on('change', function() {
            var bIDs = $(this).val();
            if(bIDs) {
                $.ajax({
                    url: "../ajaxBuilderProject/"+bIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                          //alert(value['name'])
                           $('select[name="project_id"]').append('<option value="'+ values['id'] +'">'+ values['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });


    });*/
    </script>

<script>
  //check checkbox work onload
if ($("#check").is(':checked')){
 
$("#loan_amount").prop('disabled',false);
$("#monthly_emi").prop('disabled',false);
$("#emi_starts_at").prop('disabled',false);
}
  //loan available work onchange 
$(function(){
$("#check").change(function(){
var str=!this.checked;
if(str){
$("#loan_amount").prop('disabled',true);
$("#monthly_emi").prop('disabled',true);
$("#emi_starts_at").prop('disabled',true);
}
else{
$("#loan_amount").prop('disabled',false);
$("#monthly_emi").prop('disabled',false);
$("#emi_starts_at").prop('disabled',false);
}
});
});

</script>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--Footer-part-->
@include('backend.layouts.footer')
