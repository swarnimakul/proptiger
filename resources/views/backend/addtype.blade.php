@include('backend.layouts.master')

<!--Header-part to commit-->

  @include('backend.layouts.header')
  

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}" class="tip-bottom">Properties</a> <a href="{{ URL::to('backend/add-prop-type').'/' }}" class="current">{{!empty($propertytype)?'Update Property Type':'Add Property Type'}}</a> </div>
  <h1>{{!empty($propertytype)?'Update Property Type':'Add Property Type'}}</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{!empty($propertytype)?'Update Property Type':'Add Property Type'}}</h5>
        </div>
       @if(!empty($message))
        @if($message=='fail')
        <div class="alert alert-danger">Property type already Exists</div>
        @endif
         @if($message=='success')
        <div class="alert alert-success">Property type added successfully!</div>
        @endif
      @endif
        <div class="widget-content nopadding">
             @include('backend.layouts.errors')
         <form  method="post" action="" >
          {{ csrf_field() }}
          <div class="form-horizontal">
           
          <div class="control-group">
              <label class="control-label">Property Type:</label>
              <div class="controls">
               <input class="span4" type="text" name="prop-type" value="{{!empty($propertytype->name)?$propertytype->name:''}}" requird >
              </div>
          </div>
          <div class="form-actions">
             <label class="control-label"></label>
              <button type="submit" class="btn btn-success">{{!empty($propertytype)?'Update':'Save'}}</button>
          </div>
          </div>
          </form>
        </div>
      </div>
</div>
 
</div>

</div></div></div>
<!--Footer-part-->
@include('backend.layouts.footer')
