@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 
<style type="text/css">
   .icon-remove {
    color: red;
}
.icon-ok-sign{
  color:green;
}
</style>

<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> 
      <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}">Properties</a> <a href="{{ URL::to('backend/home-loan').'/' }}" class="current">Home Loan</a> </div>
    <h1>Applicant List For Home Loan</h1>
  </div>
   @if(session('info')=='success')
  <div class="alert alert-success">Location Updated Successfully!</div>
  @endif
  @if(session('info')=='fail')
  <div class="alert alert-danger">Location already exists</div>
  @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-plus"></i></span>
            <h5><a href="">Applicant List For Home Loan </a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Applicant Name</th>
                  <th>Country</th>
                    <th>Email</th>
                  <th>Phone Number</th>
                  <th>City Of Property</th>
                  <th>Identified Property</th>
                  <th>Occupation Type</th>
                  <th>Loan Amount in Lacs</th>
                  </tr>
              </thead>
              <tbody>
                @if(count($usersappied)>0)
              @foreach( $usersappied as $userappied )
                      
                <tr class="gradeU">
                  <td>{{ $userappied->applicant_name }}</td>
                     <td>{{ $userappied->country }}</td>
                     <td>{{ $userappied->email }}</td>
                       <td>{{ $userappied->phone }}</td>
                         <td>{{ $userappied->city_of_property }}</td>
                   <td>{{ $userappied->identified_property }}</td>
                             <td>{{ $userappied->occupation_type }}</td>
                       <td>Rs.{{ $userappied->loan_amount_in_lacs}} Lacs</td>
                 
                  </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function update_status(status,u_id){
    var status = status;
    var u_id=u_id;
    var msg='';
    var url="{{url('/')}}/backend/locations/{uid}";
    if(status=='yes'){
    msg=' Deactive';
  }
  else{
    msg=' Active';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {status:status,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
<!--Footer-part-->
@include('backend.layouts.footer')


