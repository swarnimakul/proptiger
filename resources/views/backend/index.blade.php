@extends('backend.layouts.master')

@section('content')
<div class="page">

  @include('backend.layouts.header')

  
    @include('backend.layouts.sidebar')  
  
    <!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="{{ URL::to('backend/index').'/' }}"> <i class="icon-dashboard"></i> <span class="label label-important">20</span> My Dashboard </a> </li>
        <li class="bg_lg span3"> <a href="{{ URL::to('backend/properties').'/' }}"> <i class="icon-signal"></i><span class="label label-success">{{AllPurposeController::getPropertyCount()}}</span>Properties</a> </li>
        <li class="bg_ly"> <a href="{{ URL::to('backend/projects').'/' }}"> <i class="icon-signal"></i><span class="label label-success">{{AllPurposeController::getProjectCount()}}</span> Projects </a> </li>
        <li class="bg_lo"> <a href="{{ URL::to('backend/builders').'/' }}"> <i class="icon-signal"></i> <span class="label label-success">{{AllPurposeController::getBuilderCount()}}</span>Builders</a> </li>
        <li class="bg_ls"> <a href="{{ URL::to('backend/pages').'/' }}"> <i class="icon-signal"></i> <span class="label label-success">{{AllPurposeController::getPageCount()}}</span> Pages</a> </li>
        <li class="bg_lo span3"> <a href="{{ URL::to('backend/amenities').'/' }}"> <i class="icon-signal"></i> <span class="label label-success">{{AllPurposeController::getAmenityCount()}}</span>Amenities</a> </li>
        <li class="bg_ls"> <a href="{{ URL::to('backend/locations').'/' }}"> <i class="icon-tint"></i><span class="label label-success">{{AllPurposeController::getLocationCount()}}</span>Locations</a> </li>
        <li class="bg_lb"> <a href="{{ URL::to('backend/categories').'/' }}"> <i class="icon-pencil"></i><span class="label label-success">{{AllPurposeController::getCategoryCount()}}</span>Categories</a> </li>
        <li class="bg_lg"> <a href="{{ URL::to('backend/prop-type').'/' }}"> <i class="icon-calendar"></i><span class="label label-success">{{AllPurposeController::getPropertyTypeCount()}}</span>Property Types</a> </li>
      </ul>
    </div>
<!--End-Action boxes-->    


           
        <div class="widget-box">
          <div class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon"> <i class="icon-chevron-down"></i> </span>
            <h5>User Queries</h5>
          </div>
          <div class="widget-content nopadding updates collapse in" id="collapseG3">
            <!--Queries section  -->
            @if(!empty($user_Queries))
            @foreach($user_Queries as $query)
            <div class="new-update clearfix"><i class="icon-leaf"></i>
              <div class="update-done"><a title="" href="#"><strong>{{$query->query}}
              </strong></a> <span>By:{{$query->name}}/Email:{{$query->email}}/Phone:{{$query->phone}}</span> </div>
              <div class="update-date"><span class="update-day">{{date("d",strtotime($query->created_at))}}</span>{{date("M",strtotime($query->created_at))}}</div>
            </div>
            @endforeach
            @else
             <div class="new-update clearfix"> <i class="icon-leaf"></i> <span class="update-notice"> <a title="" href="#"><strong>No Queries </strong></a> <span>many many happy returns of the day</span> </span> <span class="update-date"><span class="update-day">11</span>jan</span> </div>
            @endif
           
<!-- close Queries -->
           <!-- <div class="new-update clearfix"> <i class="icon-gift"></i> <span class="update-notice"> <a title="" href="#"><strong>Congratulation Maruti, Happy Birthday </strong></a> <span>many many happy returns of the day</span> </span> <span class="update-date"><span class="update-day">11</span>jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-move"></i> <span class="update-alert"> <a title="" href="#"><strong>Maruti is a Responsive Admin theme</strong></a> <span>But already everything was solved. It will ...</span> </span> <span class="update-date"><span class="update-day">07</span>Jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-leaf"></i> <span class="update-done"> <a title="" href="#"><strong>Envato approved Maruti Admin template</strong></a> <span>i am very happy to approved by TF</span> </span> <span class="update-date"><span class="update-day">05</span>jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-question-sign"></i> <span class="update-notice"> <a title="" href="#"><strong>I am alwayse here if you have any question</strong></a> <span>we glad that you choose our template</span> </span> <span class="update-date"><span class="update-day">01</span>jan</span> </div> -->
          </div>
        </div>
        
      </div>
      
  </div>
</div>

<!--end-main-container-part-->
@include('backend.layouts.footer')
@endsection