@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<style type="text/css">
  .img{
    width:100px;
  }
</style>
<!--Header-part-->
  @include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menu-->
@include('backend.layouts.sidebar')  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/subdomains').'/' }}" >Subdomains</a> <a href="{{ URL::to('backend/add-subdomain').'/' }}" class="current"> {{!empty($subdomains)?'Update':'Add'}} subdomain </a> </div>
  <h1>{{!empty($subdomains)?'Update':'Add New'}} Subdomain</h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>{{!empty($subdomains)?'Update':'Add'}} </h5>
          </div>
          @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="#"  enctype="multipart/form-data"  >
                {{ csrf_field() }}
                @include('backend.layouts.errors')
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input type="text" name="name" required="" value="{{!empty($subdomains)?$subdomains->name:''}}" >
                </div>
              </div>
          
             <div class="control-group">
                <label class="control-label" >Url</label>
                <div class="controls">
                 <input type="url" name="url"  value="{{!empty($subdomains)?$subdomains->url:''}}"  >
                </div>
             </div>
             <div class="control-group">
                <label class="control-label">Logo</label>
                <div class="controls">
                   <input type="file" id="image" value="{{!empty($subdomains)?$subdomains->logo:''}}" name="logo"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src="{{!empty($subdomains->logo)?'../../public/upload/subdomain_logo/'.$subdomains->logo:''}}"
    >
                </div>
              </div>
              <!-- brochure -->
<div class="control-group">
<label class="control-label">Brochure</label>
<div class="controls">
<input type="file" accept="application/pdf" value="{{!empty($subdomains)?$subdomains->brochure:''}}" name="brochure"  />
@if(!empty($subdomains->brochure))
<a  href="../../public/upload/subdomain_brouchers/{{$subdomains->brochure}}" target="_blank">{{$subdomains->brochure}}</a>
 @endif
</div>
</div>
              <!-- close brochure -->
            <div class="widget-box">
            <div class="widget-title"> 
              <h5>Contact Us Details</h5>
            </div>
            <div class="widget-content">
              <div class="control-group">
                <label class="control-label" >Email</label>
                <div class="controls">
                    <input type="email" name="email"   value="{{!empty($subdomains)?$subdomains->email:''}}" >
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" >Phone</label>
                <div class="controls">
                    <input type="phone" name="phone"   value="{{!empty($subdomains)?$subdomains->phone:''}}"  >
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" >Address</label>
                <div class="controls">
                   <textarea name="address">{{!empty($subdomains)?$subdomains->address:''}}</textarea>
                </div>
              </div>
            </div>
            <div class="widget-box">
            <div class="widget-title"> 
              <h5>Home Page Content</h5>
            </div>
            <div class="widget-content">
               <div class="control-group"  >
                <div class="">
                     <textarea cols="3" id="textarea"  name="home_page_content" required="">{{!empty($subdomains)?$subdomains->home_page_content:''}}</textarea>
             
                </div>
              </div>
            </div>
          </div>
          <div class="widget-box">
            <div class="widget-title"> 
              <h5>About us Content</h5>
            </div>
            <div class="widget-content">
               <div class="control-group"  >
                <div class="">
                     <textarea cols="3" id="textarea1"  name="about_us_content" required="">{{!empty($subdomains)?$subdomains->about_us_content:''}}</textarea>
             
                </div>
              </div>
            </div>

          
              <div class="form-actions">
              <button type="submit" class="btn btn-success">{{!empty($subdomains)?'Update':'Save'}}</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

    <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
 <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea1');</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--Footer-part-->
@include('backend.layouts.footer')
