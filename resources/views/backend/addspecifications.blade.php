@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--Header-part-->
@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> 
      <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
      <a href="{{ URL::to('backend/properties').'/' }}">Property</a>
      <a href="{{ URL::to('backend/add-specifications').'/' }}" class="current">Add Specification</a>

     </div>
  <h1>Add Specification </h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>Add</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"  >
                {{ csrf_field() }}
                @include('backend.layouts.errors')
                 @if(session('info'))
<div class="alert alert-danger">{{session('info')}}</div>
  @endif
               <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input class="span4" type="text" name="specification" value="" requird >
                </div>
              </div>
              @if(isset($specification_name) && !empty($specification_name))
              <div class="control-group">
                 <label class="control-label">Parent Specification</label>
                <div class="controls">
                    <input type="text" name="" value="{{$specification_name}}" disabled="">
                    <input type="hidden" name="id" value="{{$specification_id}}" disabled="">
                 
                 </div>
              </div>
              <div class="control-group" id="selectCat">
                 <label class="control-label">Feature</label>
                  <div class="controls">
                    <input type="text" placeholder="eg:Flush Shutters,Wooden gate" name="feature">
                  </div>
              </div>
              @endif
             <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Save</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
       </div>
      </div>
 

       <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    
<!--Footer-part-->
@include('backend.layouts.footer')
