<!--sidebar-menu-->
<div id="sidebar"><a href="{{ URL::to('backend/index').'/' }}" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="{{ ( $sidebarTab == 'Home' ) ? 'active' : '' }}"><a href="{{ URL::to('backend/index').'/' }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="{{ ( $sidebarTab == 'Pages' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/pages').'/' }}"><i class="icon icon-th"></i> <span>Pages</span></a>
        <ul>
            <li><a href="{{ URL::to('backend/pages').'/' }}">Pages</a></li>
        </ul>
        <ul>
            <li><a href="{{ URL::to('backend/add-home-page').'/' }}">Add Home Page</a></li>
        </ul>
        <ul>
            <li><a href="{{ URL::to('backend/add-page').'/' }}">Add Page</a></li>
        </ul>
    </li>
   <!--<li class="{{ ( $sidebarTab == 'Projects' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/projects').'/' }}"><i class="icon icon-file"></i> <span>Projects</span></a>
    	<ul>
        <li><a href="{{ URL::to('backend/projects').'/' }}">Project List</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-projects').'/' }}">Add Project</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/categories').'/' }}">Category</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/locations').'/' }}">Location</a></li>
        </ul>
    </li>-->
    <li class="{{ ( $sidebarTab == 'Properties' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/properties').'/' }}"><i class="icon icon-file"></i> <span>Properties</span></a>
       <ul>
        <li><a href="{{ URL::to('backend/properties').'/' }}">Property List</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-property').'/' }}">Add Property</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/amenities').'/' }}">Amenities</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/prop-type').'/' }}">Property type</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-prop-price').'/' }}">Add Property Price</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/categories').'/' }}">Category</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/locations').'/' }}">Location</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/specifications').'/' }}">Specifications</a></li>
        </ul>

    </li>
    <li class="{{ ( $sidebarTab == 'Builders' ) ? 'active' : '' }}"><a href="{{ URL::to('backend/builders').'/' }}"><i class="icon icon-th"></i> <span>Builders</span></a></li>

  <li class="{{ ( $sidebarTab == 'Subdomains' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/subdomains').'/' }}"><i class="icon icon-file"></i> <span>Subdomains</span></a>
       <ul>
        <li><a href="{{ URL::to('backend/subdomains').'/' }}">Subdomains List</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-subdomain').'/' }}">Add Subdomain</a></li>
        </ul>
        <ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-subdomain-price').'/' }}">Add  Price List</a></li>
        </ul>

    </li>
    
  </ul>
</div>
<!--sidebar-menu-->