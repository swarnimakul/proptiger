<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2018 &copy;  <a href="http://dehlismartcities.com">Delhismartcities.com</a> </div>
</div>

<!--end-Footer-part-->

<!--Google MAp-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsMVfio3e-dKTp1S4etAdOoJDZrqnWEfM&libraries=places"></script>

<script src="{{asset('public/js/jquery.geocomplete.js')}}"></script>
<script src="{{asset('public/js/logger.js')}}"></script>
@if (isset($propertyList->latitude) && $propertyList->latitude !='')
    @php $lat = $propertyList->latitude @endphp    
@else
    @php $lat = '28.6139391' @endphp
@endif
@if (isset($propertyList->longitude) && $propertyList->longitude !='')
    @php $lng = $propertyList->longitude @endphp    
@else
    @php $lng = '77.20902120000005' @endphp
@endif
    <script>
      $(function(){


       var options = {
          map: ".map_canvas",
          details: "#info",
          location: ["{{$lat}}","{{$lng}}"],
          types: ["geocode", "establishment"],

       };  
      $("#geocomplete").geocomplete(options);
   
      });
    </script>
<!--End Google MAp -->
<script src="{{asset('public/backend/js/bootstrap.min.js')}}"></script> 

<script src="{{asset('public/backend/js/jquery.uniform.js')}}"></script> 
<script src="{{asset('public/backend/js/select2.min.js')}}"></script> 
<script src="{{asset('public/backend/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.validate.js')}}"></script> 

<script src="{{asset('public/backend/js/matrix.js')}}"></script> 
<script src="{{asset('public/backend/js/matrix.tables.js')}}"></script>
<script src="{{asset('public/backend/js/matrix.form_validation.js')}}"></script>

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>




<script src="{{asset('public/backend/js/jquery.peity.min.js')}}"></script> 

</body>
</html>
