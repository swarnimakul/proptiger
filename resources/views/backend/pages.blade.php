@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 
<style type="text/css">
   .icon-remove {
    color: red;
}
.icon-ok-sign{
  color:green;
}
</style>

<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/pages').'/' }}" class="current">Pages</a> </div>
    <h1>Pages</h1>
  </div>
    @if(!empty(session('info')))
    <div class="alert alert-success">{{session('info')}}</div>
    @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5><a href="{{ URL::to('backend/add-page').'/' }}">Add Pages</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>

                  <th>Action</th>
                  <th>Added On</th>
                </tr>
              </thead>
              <tbody>
                @if(count($pageList)>0)
              @foreach( $pageList as $page )
                      
                <tr class="gradeU">
                  <td>{{ $page->title }}</td>
                  <td>
                     @if($page->is_active == 'no')
                    <button class="btn btn-danger btn-mini" onclick="update_status('{{ $page->is_active }}',{{ $page->id }})">Deavtive</button>
                    @endif
                     @if($page->is_active == 'yes')
                   
                    <button class="btn btn-success btn-mini" onclick="update_status('{{ $page->is_active }}',{{ $page->id }})">Active</button>

                    @endif
                    
                    </td>
                  <td><a href="{{ url('/' . $page->url) }}" title="View" target="_blank"><i class="icon-eye-open"></i></a> | 
                            <a href="{{ URL::to('backend/update-page').'/' }}{{ $page->id }}" title="Edit"><i class="icon icon-pencil"></i></a></td>
                  <td class="center">{{ $page->created_at }}</td>
                </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function update_status(status,u_id){
    var status = status;
    var u_id=u_id;
    var msg='';
    var url="{{url('/')}}/backend/pages/{uid}";
    if(status=='yes'){
    msg=' Deactive';
  }
  else{
    msg=' Active';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url: url,
      type: "GET",
      data: {status:status,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
<!--Footer-part-->
@include('backend.layouts.footer')


