@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 
<style type="text/css">
   .icon-remove {
    color: red;
}
.icon-ok-sign{
  color:green;
}
</style>

<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> 
      <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}">Properties</a> <a href="{{ URL::to('backend/prop-type').'/' }}" class="current">Property Type</a> </div>
    <h1>Property Type List</h1>
  </div>
  @if(session('info')=='success')
  <div class="alert alert-success">Property Updated Successfully!</div>
  @endif
  @if(session('info')=='fail')
  <div class="alert alert-danger">Property already exists</div>
  @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-plus"></i></span>
            <h5><a href="{{ URL::to('backend/add-prop-type').'/' }}">Property Type</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>

                  <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @if(count($propertyList)>0)
              @foreach( $propertyList as $property )
                      
                <tr class="gradeU">
                  <td>{{ $property->name }}</td>
                  <td>
                       @if($property->is_active == 'no')
                    <button class="btn btn-danger btn-mini" onclick="update_status('{{ $property->is_active }}',{{ $property->id }})">Deactive</button>
                    @endif
                     @if($property->is_active == 'yes')
                   
                    <button class="btn btn-success btn-mini" onclick="update_status('{{ $property->is_active }}',{{ $property->id }})">Active</button>

                    @endif
                    
                   
                    </td>
                  <td>
                            <a href="{{ URL::to('backend/update-prop-type').'/' }}{{$property->id}}" title="Edit"><i class="icon icon-pencil"></i></a></td>
                  </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function update_status(status,u_id){
    var status = status;
    var u_id=u_id;
    var msg='';
    var url="{{url('/')}}/backend/prop-type/{uid}";
    if(status=='yes'){
    msg=' Deactive';
  }
  else{
    msg=' Active';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {status:status,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
<!--Footer-part-->
@include('backend.layouts.footer')


