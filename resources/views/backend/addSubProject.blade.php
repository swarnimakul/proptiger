@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<style type="text/css">
  .img{
    width:50px;
  }
</style>
<!--Header-part-->
  @include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menu-->
@include('backend.layouts.sidebar')  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/subdomains').'/' }}" >Subdomains</a> <a href="{{ URL::to('backend/add-subdomain-projects').'/' }}{{!empty($subdomainId)?$subdomainId:''}}" class="current">{{!empty($subdomainsprojects)?'Update Project':'Add Project'}} </a> </div>
  <h1>{{!empty($subdomainsprojects)?'Update Project for '.$subdomainsprojects->name:'Add Project for '}}{{!empty($subdomainName)?$subdomainName:''}} </h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>{{!empty($subdomainsprojects)?'Update':'Add'}} </h5>
          </div>
          @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="#"  enctype="multipart/form-data"  >
                {{ csrf_field() }}

                @include('backend.layouts.errors')
               
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input type="text" name="name" required="" value="{{!empty($subdomainsprojects)?$subdomainsprojects->name:''}}" >
                </div>
              </div>
          
             <div class="control-group">
                <label class="control-label" >Website</label>
                <div class="controls">
                 <input type="text" name="url" required value="{{!empty($subdomainsprojects)?$subdomainsprojects->website:''}}"  >
                </div>
             </div>
             <div class="control-group">
                <label class="control-label">Image</label>
                <div class="controls">
                   <input type="file" id="image" value="{{!empty($subdomainsprojects)?$subdomainsprojects->image:''}}" name="image"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src="{{!empty($subdomainsprojects->image)?'../../public/upload/subdomain_projects/'.$subdomainsprojects->image:''}}"
    >
                </div>
              </div>
            <input type="hidden" name="sid" value="{{!empty($subdomainsprojects->subdomain_id)?$subdomainsprojects->subdomain_id:''}}">
            <div class="form-actions">
              <button type="submit" class="btn btn-success">{{!empty($subdomainsprojects)?'Update':'Save'}}</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--Footer-part-->
@include('backend.layouts.footer')
