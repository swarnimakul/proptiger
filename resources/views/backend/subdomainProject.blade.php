@include('backend.layouts.master')
<!--Header-part-->
@include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<style type="text/css">
  .img{
    width:50px;
  }
</style>
<!--close-Header-part--> 
<!--sidebar-menu-->
@include('backend.layouts.sidebar')  
  <div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/subdomains').'/' }}" class="tip-bottom" >Subdomains</a><a class="current">{{!empty($subdomainstitle)?$subdomainstitle->subd_name:''}} Projects</a> </div>
    <h1>{{!empty($subdomainstitle)?$subdomainstitle->subd_name:''}} Projects</h1>
  </div>
  <div class="container-fluid">
    <hr>
     @if(!empty(session('info')))
  <div class="alert alert-success">{{session('info')}}</div>
    @endif
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5><a href="{{ URL::to('backend/add-subdomain-projects').'/' }}{{!empty($subdomainstitle)?$subdomainstitle->id:''}}">Add {{!empty($subdomainstitle)?$subdomainstitle->subd_name:''}}  Projects</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(!empty($projects))
                @foreach($projects as $project)
               <tr>
                 <td>{{$project->name}}</td>
                   <td><img class="img" src="../../public/upload/subdomain_projects/{{$project->image}}"></td>
                       <td><a href="{{ url('/' . $project->url) }}" title="View" target="_blank"><i class="icon-eye-open"></i></a> | 
                            <a href="{{ URL::to('backend/update-subdomain-projects').'/' }}{{ $project->spid }}" title="Edit"><i class="icon icon-pencil"></i></a></td>
               </tr>
               @endforeach
               @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->

@include('backend.layouts.footer')


