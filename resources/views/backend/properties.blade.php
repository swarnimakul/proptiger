@include('backend.layouts.master')

<!--Header-part-->
@include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 
<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/properties').'/' }}" class="current">Properties</a> </div>
    <h1>Property List</h1>
  </div>
    @if(session('info'))
<div class="alert alert-success">{{session('info')}}</div>
  @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-plus"></i></span>
            <h5><a href="{{ URL::to('backend/add-property').'/' }}">Add Property</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                   <th>Builder Name</th>
                   <th>Location</th>
                   <th>Price</th>
                  
                  <th>Status</th>
                  
                  <th>Action</th>
                  
                </tr>
              </thead>
              <tbody>
              @if(count($projpertyList)>0)
              @foreach( $projpertyList as $property )
                      
                <tr class="gradeU">
                 <td>{{ $property->property_name }}</td>
                  <td class="center">{{ !empty($property->builder_name)?$property->builder_name:'--'}}</td>
                   <td class="center">{{ $property->location}}</td>
                  <td>{{ $property->price }}</td>
                
                   <td>
                     @if($property->is_active == 'no')
                    <button class="btn btn-danger btn-mini" onclick="update_status('{{ $property->is_active }}',{{ $property->id }})">Deactive</button>
                    @endif
                     @if($property->is_active == 'yes')
                   
                    <button class="btn btn-success btn-mini" onclick="update_status('{{ $property->is_active }}',{{ $property->id }})" >Active</button>

                    @endif
                  </td>
                  <td >
                    <a href="{{url('/')}}" target="_blank" title="View"><i class="icon-eye-open" ></i></a> | 
                    <a href="{{url('/')}}/backend/update-property/{{ $property->id }}" title="Edit"><i class="icon-edit"></i></a> | 
                    <a href="{{url('/')}}/backend/update-property/{{ $property->id }}" title="View Property">(0) Properties</a>
                  </td>
                 
                </tr>
              @endforeach
            @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function update_status(status,u_id){
    var status = status;
    var u_id=u_id;
    var msg='';
      var url="{{url('/')}}/backend/properties/{uid}";
    if(status=='yes'){
    msg=' Deactive';
  }
  else{
    msg=' Active';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {status:status,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
<!--Footer-part-->
@include('backend.layouts.footer')


