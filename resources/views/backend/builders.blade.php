@include('backend.layouts.master')

<!--Header-part-->


  @include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/builders').'/' }}" class="current">Builders</a> </div>
    <h1>Builders List</h1>
  </div>
  <div class="container-fluid">
    <hr>
     @if(!empty(session('info')))
    <div class="alert alert-success">{{session('info')}}</div>
    @endif
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5><a href="{{ URL::to('backend/add-builder').'/' }}">Add builders</a></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>

                  <th>Action</th>
                 <th></th>
                </tr>
              </thead>
              <tbody>
                @if(count($builderList)>0)
              @foreach( $builderList as $builder )
                      
                <tr class="gradeU">
                  <td>{{ $builder->name }}</td>
                  <td>
                    @if($builder->is_verified == 0)
                    <button class="btn btn-danger btn-mini" onclick="updateBuilderStatus({{ $builder->is_verified }},{{ $builder->id }})">Deactive</button>
                    @endif
                     @if($builder->is_verified == 1)
                   
                    <button class="btn btn-success btn-mini" onclick="updateBuilderStatus({{ $builder->is_verified }},{{ $builder->id }})">Active</button>

                    @endif
                  </td>
                  
                  <td >
                    <a href="{{ url('/' . $builder->url) }}" target="_blank" title="View"><i class="icon-eye-open" ></i></a> | 
                    <a href="{{ URL::to('backend/update-builder').'/' }}{{ $builder->id }}" title="Edit"><i class="icon-edit"></i></a>

                  </td>
                  <td >
                  <a href="{{ URL::to('backend/add-projects').'/' }}{{$builder->id}}" title="add">Add Project  </a>

                  |
                  (<a href="{{ URL::to('backend/projects/bid').'=' }}{{ $builder->id }}">{{BuildersController::getProjects($builder->id)}}</a>) Projects
                 </td>
                </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<script type="text/javascript">
  function updateBuilderStatus(is_verified,u_id){
    var is_verified = is_verified;
    var u_id=u_id;
    var msg='';
     var url="{{url('/')}}/backend/builders/{uid}";
    if(is_verified==1){
    msg=' Deactive';
  }
  else{
    msg=' Activate';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {is_verified:is_verified,u_id:u_id},
      success: function(value){
        location.reload();
      }
    });
  }
  }

</script>
@include('backend.layouts.footer')


