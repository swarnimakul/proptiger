<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\UserQuery;
class AllPurposeController extends Controller
{
    public function index() {
        $sidebarTab = 'Home';
     $user_Queries=UserQuery::all();
     
        return view('backend.index', compact('sidebarTab','user_Queries'));
    }

    public function login() {
        
        if ( auth()->check() ) {
            return $this->index();
        }


        return view('backend.login');
    }

    public function doLogin(Request $request) {
        $rememberMe = 1;
        //$rememberMe = ! empty(request('rememberMe')) ? 1 : 0;

        if ( auth()->attempt([
                'email' => $request->email, 
                'password' => $request->password
            ], $rememberMe) ) {

            // Authentication passed...
            return redirect()->route('backendIndex');
        } else {
            $response = [
                'message' => 'User verification failed.',
                'status'  => false
            ];
        }

        return redirect()->route('backendLogin')->withErrors(['msg'=>'Unauthorised user access detected.']);
    }

    public function logout() {
        if( auth()->check() ) {
            auth()->logout();

            // Flushing all session
            Session::flush();
        }

        // redirect to home page
        return redirect()->route('backendLogin');
    }

    public static function getPropertyCount(){
         $result=DB::table('properties')->count('id');
            echo $result;
    }
    public static function getProjectCount(){
          $result=DB::table('projects')->count('id');
            echo $result;
    }
    public static function getBuilderCount(){
          $result=DB::table('builders')->count('id');
            echo $result;
    }
     public static function getAmenityCount(){
          $result=DB::table('amenities')->count('id');
            echo $result;
    }
    public static function getLocationCount(){
          $result=DB::table('locations')->count('id');
            echo $result;
    }
     public static function getPageCount(){
          $result=DB::table('pages')->count('id');
            echo $result;
    }
 public static function getCategoryCount(){
          $result=DB::table('categories')->count('id');
            echo $result;
    }
    public static function getPropertyTypeCount(){
          $result=DB::table('property_types')->count('id');
            echo $result;
    }
}
