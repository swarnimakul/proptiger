<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AllPurposeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomePageController extends Controller
{
	public function index() {
		$apc = new AllPurposeController();
		//$apc->payForQry();
		
		return view('index');
	}

	
}
