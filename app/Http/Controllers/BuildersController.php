<?php

namespace App\Http\Controllers;

use App\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BuildersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function index()
    {
        $request = request()->all();
        //$builders = Builder::where('state_id', $request['stateId'])->get();

        if( empty(Session::get('qryParams.defaultState')) ) {
            // Setting 'New Delhi' as a default state
            Session::put('qryParams.defaultState', 'st34');
        } else {
            Session::put('qryParams.defaultState', $request['stateId']);
        }

        // Removing characters from state id.
        Session::put('qryParams.stateId', filter_var(Session::get('qryParams.defaultState'), FILTER_SANITIZE_NUMBER_INT));

        $builders = Builder::BuildersFromState($request['stateId']);

        return response()->json($builders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $builders = Builder::create(['name' => $request->newBuilder, 'status' => 'Inactive']);
        return response()->json($builders);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
