<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserQuery;

class UserQueriesController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getUsersQuery()
    {
      $user_Queries=UserQuery::all();
      return $user_Queries;
      
    }

}