<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\PropertyPrice;

class PropertyPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Properties';
        $properties = DB::table('properties')
                        ->select('name','id')
                        ->orderBy('name', 'Asc')
                        ->get();
          
        return view('backend.addPropPrice', compact('sidebarTab','properties'));
    }


 public function add()
    {
        $sidebarTab = 'Properties';

        return view('backend.addtype', compact('sidebarTab'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //store property price list
  public function store(Request $request)
  { 
          
    	$sidebarTab = 'Properties';
         $request->validate([
            'property_id' => 'required',
         ]);

       //save property price list array
       $propertypricearray=array();

        //for multiple record entry
       for($i=0;$i<count($request['unit_size']);$i++){
              $propertyprice = new PropertyPrice;
              $propertyprice->property_id = $request->property_id;
              $propertyprice->accomodation_type = $request['accomadation_type'][$i];
              $propertyprice->unit_size   = $request['unit_size'][$i];
              $propertyprice->basic_price = $request['basic_price'][$i];
              $propertyprice->total_price = $request['total_price'][$i];

              $propertypricearray[] = $propertyprice->attributesToArray();
        }
        ///fetch list
      foreach ($propertypricearray as $list) {
        PropertyPrice::insert($list); 
      }
      return redirect('backend/add-prop-price')->with('message','success');
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Properties';
        $propertytype = PropertyType::find($id);
        return view('backend.addtype', compact('sidebarTab', 'propertytype'));
    }


public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $message='fail';
        $request->validate([
            'prop-type' => 'required',
           
        ]);
     $propertycheck = PropertyType::where('name', $request['prop-type'])->where('id','!=',$id)->first();
        if ($propertycheck === null) {
         // category doesn't exist
          $proptype =PropertyType::find($id);
        $proptype->name = $request['prop-type'];
        $proptype->update();
        return redirect('backend/prop-type')->with('info','success');
    }
    return redirect('backend/prop-type')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 PropertyType::where('id',$id)->update($data);  

}
}