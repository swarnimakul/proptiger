<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\SubdomainProject;
use App\SubdomainPrice;
use App\Subdomain;

class SubdomainsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Subdomains';
        $subdomainList = Subdomain::all();
        return view('backend.subdomains', compact('sidebarTab', 'subdomainList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $sidebarTab = 'Subdomains';
        return view('backend.addsubdomain', compact('sidebarTab'));
    }

    public function addProject($subdomainId)
    {
        $sidebarTab = 'Subdomains';
        $subdomain = DB::table('subdomains')
                        ->select('name')
                        ->where('id' , '=' , $subdomainId)
                        ->first();
        $subdomainName = $subdomain->name;
        return view('backend.addSubProject', compact('sidebarTab','subdomainId','subdomainName'));

    }

    public function storeProject(Request $request,$subdomainId)
    {
        $request->validate([
            'name' => 'required',
            'url'=>'required',
            'image'=>'image|mimes:jpg,png,jpeg,gif',
        
           ]);
                  
        $projectExist = DB::table('subdomain_projects')->select('name')
                        ->where('name' , '=' , $request['name'])
                        ->where('subdomain_id' , '=' , $request['id'])
                        ->first();
        if(!empty($projectExist)){
            return redirect('backend/add-subdomain-projects')->with('info',
           'Project Name Already Exist');
        }else{
           
            if($request->hasFile('image')){
              $extension=$request->file('image')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
              $request->file('image')->move(public_path('upload/subdomain_projects'),$logo);
            }else{
                $logo ='';
            }
            $project =  DB::table('subdomain_projects')
                    ->insert(
                      ['subdomain_id' => $subdomainId,
                       'name' => $request['name'],
                       'website' => $request['url'],
                       'image' => $logo
                      ]);
                
            return redirect('backend/subdomains')->with('info',
           'Record added successfully');
         }  

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'url'=>'required',
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
        
           ]);
                  
        $subdomainExist = DB::table('subdomains')->select('name')->where('name' , '=' , $request['name'])->first();
        if(!empty($subdomainExist)){
            return redirect('backend/add-subdomain')->with('info',
           'Name Already Exist');
        }else{
           
            if($request->hasFile('logo')){
              $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
              $request->file('logo')->move(public_path('upload/subdomain_logo'),$logo);
            }else{
                $logo ='';
            }
            /*brochure*/
             if($request->hasFile('brochure')){
               $extension=$request->file('brochure')->getClientOriginalExtension();
              $fileName=$request['name'].'.'.$extension;
              $request->file('brochure')->move(public_path('upload/subdomain_brouchers'),$fileName);
            }else{
                $fileName='';
            }
            /*end brochure*/
            $subdomain = new Subdomain;
            $subdomain->name = $request['name'];
            $subdomain->url  = $request['url'];
            $subdomain->home_page_content=$request['home_page_content'];
            $subdomain->about_us_content =$request['about_us_content'];
            $subdomain->logo = $logo;
            $subdomain->brochure = $fileName;
            $subdomain->address = $request['address'];
            $subdomain->email = $request['email'];
            $subdomain->phone = $request['phone'];
            
            $subdomain->save();
            return redirect('backend/subdomains')->with('info',
           'Record added successfully');
         }  
    }


    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sidebarTab = 'Subdomains';
      $subdomains = Subdomain::find($id); 
      return view('backend.addsubdomain', compact('sidebarTab','subdomains')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      $sidebarTab = 'Subdomains';
      $subdomainExist = DB::table('subdomains')->where('name','=',$request['name'])->where('id' , '!=' , $id)->first();
        if(!empty($subdomainExist)){
            return redirect('backend/update-subdomain/'.$id)->with('info',
           'Name Already Exist');
        }else{
             /*brochure*/
//subdomain brochure
          $subdomainBroch = DB::table('subdomains')->where('id',$id)->select('brochure')
                    ->first();
          if($request->hasFile('brochure')){
            $extension=$request->file('brochure')->getClientOriginalExtension();
        $BrochName=$request['name'].'.'.$extension;
        $file_path=public_path('upload\subdomain_brouchers\\'.$subdomainBroch->brochure);
          if(is_file($file_path)) {
           unlink($file_path);
          }
        $request->file('brochure')->move(public_path('upload/subdomain_brouchers'),$BrochName);
      }else{
        $BrochName = $subdomainBroch->brochure;
      }


          //end

             /*end*/
            if($request->hasFile('logo')){
                 $filename=Subdomain::where('id',$id)->first();
            $file_path=public_path('upload\subdomain_logo\\'.$filename->logo);
            if(is_file($file_path)) {

               unlink($file_path);
            }
              $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
              $request->file('logo')->move(public_path('upload/subdomain_logo'),$logo);
               $subdomain = Subdomain::find($id);
            $subdomain->name = $request['name'];
            $subdomain->url  = $request['url'];
            $subdomain->home_page_content=$request['home_page_content'];
            $subdomain->about_us_content =$request['about_us_content'];
          $subdomain->brochure=$BrochName;
            $subdomain->address = $request['address'];
            $subdomain->email = $request['email'];
            $subdomain->phone = $request['phone'];
                $subdomain->logo = $logo;
                 $subdomain->update(); 
            }
            else{
            $subdomain = Subdomain::find($id);
            $subdomain->name = $request['name'];
            $subdomain->url  = $request['url'];
            $subdomain->home_page_content=$request['home_page_content'];
            $subdomain->about_us_content =$request['about_us_content'];
           $subdomain->brochure=$BrochName;
            $subdomain->address = $request['address'];
            $subdomain->email = $request['email'];
            $subdomain->phone = $request['phone'];
            $subdomain->update();  
        }
             return redirect('backend/subdomains')->with('info',
           'Record Updated successfully');
    }
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

    public static function  getProjectCount($id){
        $NoOfProjects=0;
          if(!empty($id)){
            $NoOfProjects=DB::table('subdomain_projects')
            ->where('subdomain_id',$id)->count();
          }
          
            echo $NoOfProjects;
    }

    public  function  getProject($id){
         $sidebarTab = 'Subdomains';
            $subdomainstitle=DB::table('subdomains')
            ->select('id','name as subd_name')->where('id',$id)->first();
            $projects=DB::table('subdomain_projects')
           ->leftjoin('subdomains', 'subdomains.id', '=', 'subdomain_projects.subdomain_id')
             ->select('subdomain_projects.name','subdomain_projects.image','subdomain_projects.id as spid','subdomains.id as sid','subdomains.url')
             ->where('subdomain_id',$id)->get();
        return view('backend.subdomainProject', compact('sidebarTab', 'projects','subdomainstitle'));
       
    }

    //edit subdomain-project
  public function editproject($id){
    $sidebarTab = 'Subdomains';
    $subdomainsprojects = DB::table('subdomain_projects')             ->where('id',$id)->first();

    return view('backend.addSubProject', compact('sidebarTab','subdomainsprojects')); 
  }


//update subdomain_project
public function updateproject(Request $request,$id){
$sidebarTab = 'Subdomains';

if($request->hasFile('image')){
$filename=SubdomainProject::where('id',$id)->first();
if($filename!=null){
$file_path=public_path('upload\subdomain_projects\\'.$filename->image);

if(is_file($file_path)) {

unlink($file_path);
}
}
$extension=$request->file('image')->getClientOriginalExtension();
$logo=$request['name'].'.'.$extension;
$request->file('image')->move(public_path('upload/subdomain_projects'),$logo);

$subdomainproject = SubdomainProject::find($id);
  $subdomainproject->name=$request->name;
  $subdomainproject->image=$logo;
  $subdomainproject->website=$request->url;
  $subdomainproject->update();
  //dd($request['url']);
            }
            else{
$subdomainproject = SubdomainProject::find($id);
  $subdomainproject->name=$request->name;
  $subdomainproject->website=$request->url;
  $subdomainproject->update();
}
$sid=$request->sid;
  return redirect('backend/subdomain-projects/'.$sid)->with('info','Record Updated Successfully!');
}

//status update
public function updateSubdomainStatus(){
   $is_verified=$_GET['is_verified'];
   if($_GET['is_verified']==1){
        $is_verified=0;
   }
   else if($_GET['is_verified']==0){
        $is_verified=1;
   }
   $data=[
    'is_verified'=>  $is_verified,
  ];

    $id=$_GET['u_id'];
 Subdomain::where('id',$id)->update($data);  
}


//Get-subdomain
public function getSubdomains(){
  $sidebarTab = 'Subdomains';
        $subdomains = DB::table('subdomains')
                        ->select('name','id')
                        ->orderBy('name', 'Asc')
                        ->get();
          
        return view('backend.addsubdomainprice', compact('sidebarTab','subdomains'));
}

//add-subdomain-price
public function addSubdomainPrice(Request $request)
  { 
          
      $sidebarTab = 'Subdomains';
         $request->validate([
            'subdomain_id' => 'required',
         ]);
         //save price list array 
       $subdomainpricearray=array();
       
       
 //for multiple record entry
    for($i=0;$i<count($request['unit_size']);$i++){
              $subdomainprice = new SubdomainPrice;
              $subdomainprice->subdomain_id = $request->subdomain_id;
              $subdomainprice->accomodation_type = $request['accomadation_type'][$i];
              $subdomainprice->unit_size   = $request['unit_size'][$i];
              $subdomainprice->basic_price = $request['basic_price'][$i];
              $subdomainprice->total_price = $request['total_price'][$i];

             $subdomainpricearray[] = $subdomainprice->attributesToArray();
                  }
        ///fetch list
      foreach ($subdomainpricearray as $list) {
         SubdomainPrice::insert($list); 
      }
 
      return redirect('backend/add-subdomain-price')->with('message','success');


}


 

}