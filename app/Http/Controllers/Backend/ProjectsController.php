<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Project;
use File;
use App\Builder;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sidebarTab='Projects';


         $projectList =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.id', 'builders.name AS builder_name','locations.location','projects.location_id')
            ->get();
        //dd($projectList);     
        return view('backend.projects', compact('sidebarTab','projectList'));
    }

 public function builderProjects($id)
    {
       $sidebarTab='Projects';

        $builderName=Builder::find($id)->name;
         $projectList =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.id', 'builders.name AS builder_name','locations.location','projects.location_id')
            ->where('builder_id','=',$id)
            ->get();
        //dd($projectList);     
        return view('backend.projects', compact('sidebarTab','projectList','builderName'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($bid=null)
    {
        $sidebarTab = 'Projects';
        $builderInfo = '';
        if($bid != '')
        {
            $builderInfo = DB::table('builders')
            ->select('name','id','location_id')
            ->where('id', '=',$bid)
            ->orderBy('name', 'Asc')
            ->get();
         // print_r($builderInfo[0]->location_id);die;
           $lid = $builderInfo[0]->location_id;
          $lidArray =  explode(",",$lid);
          $i=0;
            foreach($lidArray as $locId){
            $locations[$i++] = DB::table('locations')
                ->where('id','=',$locId)
                ->orderBy('location', 'Asc')
                ->first();
            }
             //dd($locations);die;
               // DB::table('locations')->toSql();

        }else{

            $locations = DB::table('locations')
                ->orderBy('location', 'Asc')
                ->get();
                 $builderInfo ='';
       
        }
        
        $categories = DB::table('categories')
        ->orderBy('category', 'Asc')
        ->get(); 
  
        return view('backend.addprojects', compact('sidebarTab','locations','lid','bid','builderInfo','categories'));
    }
public static function  getProjLoc($locIds){
        $locIdArray=explode(',',$locIds);
            $loc='';
            $prefix = '';  
          if(!empty($locIdArray)){
              foreach ($locIdArray as $v)
               {
                $location=DB::table('locations')->where('id',$v)->first();
                  $loc.= $prefix.$location->location;
                  $prefix = ",";
                }
          }
          else{

                $location=DB::table('locations')->where('id',$projectList->location_id)->first();
           $loc=$location->location;  
        }
            echo $loc;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBuilderProject(Request $request)
    {
         
      $error='';
        $l_id = $request['l_id'];
        $sidebarTab='Projects';
        $prefix = '';
        $lid='';
         foreach($l_id as $loc => $val){
                    $lid.=  $prefix.$val;
                    $prefix = ',';
         } 
   
                $project = new Project;
                $project->name = $request->name;
                $project->location_id = $lid;
                $project->builder_id  = $request['b_id'];
                $project->category_id = $request['category'];
                $project->description=$request['description'];
                $project->save();
                   return redirect('backend/builders')->with('info','Project added successfully for selected builder');;
     
     // }
 }

    
    public function store(Request $request)
    {
        $request->validate([
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
        ]);
        $error='';
        $l_id = $request['l_id'];
        $sidebarTab='Projects';
        $prefix = '';
        $lid='';
         foreach($l_id as $loc => $val){
                    $lid.=  $prefix.$val;
                    $prefix = ',';
         } 
        $alreadyExist =  DB::table('projects')
                       // ->where('location_id','=', $lid )
                      //  ->whereRaw("find_in_set('".$lid."',location_id)")
                        ->where('name','=', $request->name )
                        ->first();

        if(!empty($alreadyExist)){
           $error ="Project name already exist for selected location";
             $bid = '';
            $locations = DB::table('locations')
                ->orderBy('location', 'Asc')
                ->get();
                 $builderInfo = '';
            $categories = DB::table('categories')
            ->orderBy('category', 'Asc')
            ->get(); 
           
         return view('backend.addprojects', compact('sidebarTab','locations','lid','bid','builderInfo','categories','error')); 
          }
         else{
 $logo ='';
           
            if($request->hasFile('logo')){
   $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;

              $request->file('logo')->move(public_path('upload/project_logo'),$logo);
            }
            $project = new Project;
            $project->name = $request->name;
            $project->location_id = $lid;
            $project->builder_id  = 0;
            $project->category_id = $request['category'];
            $project->description=$request['description'];
            $project->logo=$logo;
            $project->save();
           return redirect('backend/projects')->with('info','Recored Added Successfully!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     * edit
     */
    public function edit($id)
    {
         $sidebarTab = 'Projects';
           $edit =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            ->join('categories', 'projects.category_id', '=', 'categories.id')
            ->select('projects.name','projects.logo','projects.description', 
            'projects.location_id','categories.category','projects.category_id','locations.location','builders.name As builder_name')->distinct('locations.location')
            ->where('projects.id','=',$id)
            ->first();
         $var=explode(',', $edit->location_id);
         $locarray=array();
         foreach($var as $x){
             $locarray[]=DB::table('locations')->where('id',$x)->first();
              
         }
        

        $locations = DB::table('locations')
        ->orderBy('location', 'Asc')
        ->get();
        $category = DB::table('categories')
        ->orderBy('category', 'Asc')
        ->get();
       return view('backend.updateprojects', compact('sidebarTab', 'locarray','edit','locations','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required',
            'location' => 'required',
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
       
           ]);
         
            $loc_id='';
               if(in_array('other_loc',$request['location']) && $request['otherLoc'] !='' )  {
                $lid = DB::table('locations')->insertGetId(['location' => $request['otherLoc']]);

                $loc_id = $lid;
            }
            if(!in_array('other_loc',$request['location']) ){
              
                $prefix ='';
                foreach($request['location'] as $loc){
                   $loc_id.=  $prefix.$loc;
                    $prefix = ',';
                }
             }
            
              if($request['location']!= '' ){
                   if($loc_id!=null){
         $logo =$request->logo;
           if($request->hasFile('logo')){
            $filename=Project::where('id',$id)->first();
$file_path=public_path('upload\project_logo\\'.$filename->logo);
 if(is_file($file_path)) {
   unlink($file_path);
}
   $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
$request->file('logo')->move(public_path('upload/project_logo'),$logo);
 $project = Project::find($id);
        $project->name = $request['name'];
        $project->description=$request['description'];
        $project->location_id = $loc_id ;
        $project->category_id=$request['category'];
        $project->logo=$logo;
        $project->update();
        return redirect('backend/projects')->with('info','Record Updated Successfully!');

            }
        $project = Project::find($id);
        $project->name = $request['name'];
        $project->description=$request['description'];
        $project->location_id = $loc_id ;
        $project->category_id=$request['category'];
        $project->update();
        return redirect('backend/projects')->with('info','Record Updated Successfully!');
    }
    else{
         return redirect('backend/update-projects/'.$id)->with('info','Location is null Not Updated');
    }
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

//Get builder in drop down from ajax

    public function ajaxBuilder($id){

        $request = request()->all();
         $builders = DB::table('builders')
         ->select('name', 'id')
         ->whereRaw("find_in_set('".$id."',location_id)")
        ->orderBy('name', 'asc')->get();
       // print_r($builders);
        return response()->json($builders);
}


//status update
    public function updateProjectStatus(){
    $is_active=$_GET['is_active'];
   if($_GET['is_active']=='yes'){
      $is_active='no';

   }
   else if($_GET['is_active']=='no'){
  $is_active='yes';
   }
    $id=$_GET['u_id'];
    $data=array('id'=>$id,'active'=>$is_active);
DB::table('projects')->where('id',$id)->update(['is_active'=>  $is_active]);  

}

}