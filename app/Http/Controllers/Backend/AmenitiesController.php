<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Amenity;

class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Properties';
        $amenityList = Amenity::all();
      
        return view('backend.amenities', compact('sidebarTab','amenityList'));
    }

    public function add()
    {
        $sidebarTab = 'Properties';

        return view('backend.addamenity', compact('sidebarTab'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    { 
          
    	$sidebarTab = 'Properties';
        $message='fail';
         $request->validate([
            'amenity' => 'required',
         ]);
        $amenitycheck = Amenity::where('name', $request['amenity'])->first();
        if ($amenitycheck === null) {
         // category doesn't exist
            $message='success';
            $amenity = new Amenity;
            $amenity->name = $request['amenity'];
            $amenity->save();
        }
   return view('backend.addamenity', compact('sidebarTab','message'));
    
    
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Properties';
        $amenity = Amenity::find($id);
        return view('backend.addamenity', compact('sidebarTab', 'amenity'));
    }

public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $request->validate([
            'amenity' => 'required',
           
        ]);
     $amenitycheck = Amenity::where('name', $request['amenity'])->where('id','!=',$id)->first();
        if ($amenitycheck === null) {
         // amenity doesn't exist
          $amenity =Amenity::find($id);
        $amenity->name = $request['amenity'];
        $amenity->update();
        return redirect('backend/amenities')->with('info','success');
    }
    return redirect('backend/amenities')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 Amenity::where('id',$id)->update($data);  

}


}