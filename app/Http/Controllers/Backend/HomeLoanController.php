<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HomeLoan;

class HomeLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Properties';
        $usersappied = HomeLoan::all();
      
        return view('backend.homeloans', compact('sidebarTab','usersappied'));
    }



}