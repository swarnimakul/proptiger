<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Builder;
use App\Category;

class BuildersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Builders';
        $builderList = Builder::all();
       
       
        return view('backend.builders', compact('sidebarTab', 'builderList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $sidebarTab = 'Builders';
       /* $categories = DB::table('categories')
        ->orderBy('category', 'Asc')
        ->get(); */

        $locations = DB::table('locations')
        ->orderBy('location', 'Asc')
        ->get();

        return view('backend.addbuilders', compact('sidebarTab','categories','locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description'=>'required',
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
        
           ]);
        $loc_id = '';
     
        if(in_array('other_loc',$request['location']) && $request['otherLoc'] !='' ){
                
                $lid = DB::table('locations')->insertGetId(['location' => $request['otherLoc']]);
                $loc_id = $lid;
            }if(!in_array('other_loc',$request['location']) ){
                //print_r($request['location']);die;$prefix ='';
                $prefix ='';
                foreach($request['location'] as $loc){
                    $loc_id.=  $prefix.$loc;
                    $prefix = ',';
                }
             }
        

        if($loc_id != '' ){
             $var2=explode(',', $loc_id);
             $builderCheck=DB::table('builders')->where('name',$request['name'])->pluck('location_id')->first();

         $var=explode(',', $builderCheck);
        
         foreach($var as $x){
            foreach ($var2 as $y) {
               if($x==$y)
               {
                return redirect('backend/add-builder')->with('info','Same builder with same location already exists.');
               }
            }
              
         }
         
            $builderExist=DB::table('builders')->where('name',$request['name'])->pluck('location_id')->first();
              if(!empty($builderExist)){
                    $newLocation = $builderExist.",".$loc_id;
        
                //update location query
        DB::table('builders')->where('name',$request['name'])->update(['location_id'=>$newLocation ]);
return redirect('backend/builders')->with('info',
           'Record added successfully');
              }
              else
            {



            $logo ='';
            if($request->hasFile('logo')){
               $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
              $request->file('logo')->move(public_path('upload/builder_logo'),$logo);
            }
            $builder = new Builder;
            $builder->name = $request['name'];
            $builder->description=$request['description'];
            $builder->location_id = $loc_id;
            $builder->logo = $logo;
            
            $builder->save();
            return redirect('backend/builders')->with('info',
           'Record added successfully');
            }
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sidebarTab = 'Builders';
        //$edit = Builder::find($id);
           $edit =DB::table('builders')
            ->join('locations', 'locations.id', '=', 'builders.location_id')
            //->join('categories', 'categories.id', '=', 'builders.category_id')
            ->select('builders.name','builders.logo','builders.description', 
			'builders.location_id','locations.location')->distinct('locations.location')
			->where('builders.id','=',$id)
            ->first();
			
		 $var=explode(',', $edit->location_id);
		 $locarray=array();
		 foreach($var as $x){
			 $locarray[]=DB::table('locations')->where('id',$x)->first();
			  
		 }
		

        /*$categories = DB::table('categories')
        ->orderBy('category', 'Asc')
        ->get(); */ 

        $locations = DB::table('locations')
        ->orderBy('location', 'Asc')
        ->get();
       return view('backend.addbuilders', compact('sidebarTab', 'locarray','edit','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
          $request->validate([
            'name' => 'required',
            'location' => 'required',
            'description'=>'required',
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
        
           ]);
          $loc_id='';
          //Insert other new location in location table
            if(in_array('other_loc',$request['location']) && $request['otherLoc'] !='' )  {
                $lid = DB::table('locations')->insertGetId(['location' => $request['otherLoc']]);

                $loc_id = $lid;
            }
            if(!in_array('other_loc',$request['location']) ){
               $prefix ='';
                foreach($request['location'] as $loc){
                   $loc_id.=  $prefix.$loc;
                    $prefix = ',';
                }
             }
            if($request['location']!= '' ){
                 $var2= explode(',', $loc_id);
                 $builderCheck= DB::table('builders')->where('name',$request['name'])->where('id','!=',$id)->pluck('location_id')->first();
                     if($builderCheck!=null){
                     $var=explode(',', $builderCheck);
                      foreach($var as $x){
                        foreach ($var2 as $y) {
                           if($x==$y)
                           {
                            return redirect('backend/update-builder/'.$id)->with('info','Same builder with same location already exists.');
                           }
                        }
                      }
                     }
                     if($loc_id!=null){
                        $logo ='';
   // dd($request->hasFile('logo'));
            if($request->hasFile('logo')){
            $filename=Builder::where('id',$id)->first();
            $file_path=public_path('upload\builder_logo\\'.$filename->logo);
            if(is_file($file_path)) {

               unlink($file_path);
            }
               $extension=$request->file('logo')->getClientOriginalExtension();
              $logo=$request['name'].'.'.$extension;
              $request->file('logo')->move(public_path('upload/builder_logo'),$logo);
              $builder = Builder::find($id);
        $builder->name = $request['name'];
        $builder->description=$request['description'];
        $builder->location_id = $loc_id ;
        $builder->logo = $logo ;
        $builder->update();
        return redirect('backend/builders')->with('info','Record Updated Successfully!');
            }

        $builder = Builder::find($id);
        $builder->name = $request['name'];
        $builder->description=$request['description'];
        $builder->location_id = $loc_id ;
        $builder->update();
        return redirect('backend/builders')->with('info','Record Updated Successfully!');
    }
    else{
         return redirect('backend/update-builder/'.$id)->with('info','Location is null Not Updated');
    }
    }

}
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

public static function  getProjects($builderIds){
        $NoOfProjects=0;
          if(!empty($builderIds)){
            $NoOfProjects=DB::table('projects')
            ->where('builder_id',$builderIds)->count();
          }
          
            echo $NoOfProjects;
    }

//builder

    public function builder(){
       
        return view('backend.builders');
           
}
//status update
    public function updateBuilderStatus(){
    $is_verified=$_GET['is_verified'];
   if($_GET['is_verified']==1){
      $is_verified=0;

   }
   else if($_GET['is_verified']==0){
  $is_verified=1;
   }
  $data=array('is_verified'=>  $is_verified);

    $id=$_GET['u_id'];
 Builder::where('id',$id)->update($data);  

//return redirect('backend/pages');
}
}