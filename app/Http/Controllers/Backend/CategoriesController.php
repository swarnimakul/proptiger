<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Projects';
        $categoryList = Category::all();
      
        return view('backend.categories', compact('sidebarTab','categoryList'));
    }

    
    public function add()
    {
        $sidebarTab = 'Projects';
        $categoryList = Category::all();
      
        return view('backend.addcategory', compact('sidebarTab'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    { 
          
    	$sidebarTab = 'Projects';
        $message='fail';
         $request->validate([
            'category' => 'required',
         ]);
        $categorycheck = Category::where('category', $request['category'])->first();
        if ($categorycheck === null) {
         // category doesn't exist
            $message='success';
            $categories = new Category;
            $categories->category = $request['category'];
            $categories->save();
        }else{
            
        }
   return view('backend.addcategory', compact('sidebarTab','message'));
    
    
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Properties';
        $category = Category::find($id);
        return view('backend.addcategory', compact('sidebarTab', 'category'));
    }

public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $request->validate([
            'category' => 'required',
           
        ]);
     $categorycheck = Category::where('category', $request['category'])->where('id','!=',$id)->first();
        if ($categorycheck === null) {
         // amenity doesn't exist
          $amenity =Category::find($id);
        $amenity->category = $request['category'];
        $amenity->update();
        return redirect('backend/categories')->with('info','success');
    }
    return redirect('backend/categories')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 Category::where('id',$id)->update($data);  

}

}