<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use Hash;

class UserController extends Controller
{
    //edit profile
public function edit($id){
	$sidebarTab = 'Home';
    $user=User::find($id);
    //dd($propertyList);
    return view('backend.myprofile',compact('user','sidebarTab'));

}

//update user
public function update(Request $request,$id){
	$sidebarTab = 'Home';
    $user=User::find($id);
    $user->name=$request->name;
    $user->email=$request->email;
    $user->phone=$request->phone;
    $user->address=$request->address;
    $user->latitude =$request->lat;
    $user->longitude=$request->lng;
    $user->update();
    if($user->update()){
return view('backend.index',compact('sidebarTab'));
}
else{
return view('/edit-profile/'.$id)->with('info','Error! Occured Profile Not Updated');
}

}

//////password change
public function editPassword($id){
	$sidebarTab = 'Home';
	$user=User::find($id);
	return view('backend.changepassword',compact('sidebarTab','user'));
}

//update password
public function updatePassword(Request $request,$id){
	$sidebarTab = 'Home';
	$oldpassword=User::find($id)->password;
	//check oldpassword with current password 
	if(Hash::check($request->cupassword,$oldpassword)){
		//update password
     User::where('id', $id)
           ->update(['password' => bcrypt($request->password)]);
           //return index page
    return view('backend.index',compact('sidebarTab'));
	}
	//oldpassword not match return with error message
	return redirect('backend/change-password/'.$id)->with('info','Current Password Not Match');

	
}

}
