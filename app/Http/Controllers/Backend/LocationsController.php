<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Location;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Projects';
          $locationList = Location::all();
      

        return view('backend.locations', compact('sidebarTab','locationList'));
    }

     public function add()
    {
        $sidebarTab = 'Projects';
         
        return view('backend.addlocation', compact('sidebarTab','locationList'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    { 
          
    	$sidebarTab = 'Builders';
         $message='fail';
         $request->validate([
            'location' => 'required',
           
   ]);
         $locationcheck = Location::where('location', $request['location'])->first();
    if ($locationcheck === null) {
     // Location doesn't exist
 $message='success';
      $locations = new Location;
       $locations->location = $request['location'];
        $locations->save();
        }
      
   return view('backend.addlocation', compact('sidebarTab','message'));
    
    
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Builders';
        $location = Location::find($id);
        return view('backend.addlocation', compact('sidebarTab', 'location'));
    }

public function update(Request $request,$id)
    {
        $sidebarTab = 'Builders';
        $request->validate([
            'location' => 'required',
           
        ]);
     $locationcheck = Location::where('location', $request['location'])->where('id','!=',$id)->first();
        if ($locationcheck === null) {
         // amenity doesn't exist
          $location =Location::find($id);
        $location->location = $request['location'];
        $location->update();
        return redirect('backend/locations')->with('info','success');
    }
    return redirect('backend/locations')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 Location::where('id',$id)->update($data);  

}
}