<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Pages';
        $pageList = Page::all();

        return view('backend.pages', compact('sidebarTab', 'pageList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $sidebarTab = 'Pages';
        return view('backend.addpages', compact('sidebarTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addHomePage()
    {
        $sidebarTab = 'Pages';
        return view('backend.addhomepage', compact('sidebarTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }
        $staticPage = new Page;
        $staticPage->title = $request['title'];
        $staticPage->url = $request['url'];
        $staticPage->meta_tag = $request['meta_tag'];
        $staticPage->meta_desc = $request['meta_desc'];
        $staticPage->is_active = $is_active;
        $staticPage->content = $request['content'];
       
        $staticPage->save();
        return redirect('backend/pages')->with('info','Record Added Successfully!');
    }
//store home page
public function storeHomePage(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }
            $og_image ='';

            if($request->hasFile('og_image')){
          
               $extension=$request->file('og_image')->getClientOriginalExtension();
              $og_image=$request['url'].'.'.$extension;
              $request->file('og_image')->move(public_path('upload/og_image'),$og_image);
          }
          $banner_image='';
            if($request->hasFile('banner_image')){
          
               $extension=$request->file('banner_image')->getClientOriginalExtension();
              $banner_image=$request['url'].'.'.$extension;
              $request->file('banner_image')->move(public_path('upload/banner_image'),$banner_image);
          }
        $page = new Page;
        $page->title = $request['title'];
        $page->url = $request['url'];
        $page->meta_tag = $request['meta_tag'];
        $page->meta_desc = $request['meta_desc'];
        $page->is_active = $is_active;
        $page->content = $request['content'];
        $page->og_title = $request['og_title'];
        $page->og_description = $request['og_description'];
        $page->og_url = $request['og_url'];
        $page->og_image =$og_image;
        $page->type = 'home';
        $page->left_module = $request['left_module'];
        $page->right_module = $request['right_module'];
        $page->middle_content = $request['middle_content'];
        $page->banner_image = $banner_image;
        $page->our_partner = $request['our_partner'];
        $page->location_features = $request['location_features'];
        $page->save();
        return redirect('backend/pages')->with('info','Record Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function show(StaticPage $staticPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Pages';
        
        $Page = Page::find($id);
       /* echo '<pre>';
print_r($Page);
echo '</pre>';)*/
        return view('backend.addpages', compact('sidebarTab', 'Page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request,$id)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
           
        ]);
 $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }
        $staticPage =Page::find($id);
        $staticPage->title = $request->title;
        $staticPage->url = $request->url;
        $staticPage->meta_tag = $request->meta_tag;
        $staticPage->meta_desc = $request->meta_desc;
        $staticPage->content = $request->content;
        $staticPage->is_active=$is_active;
        $staticPage->update();

        return redirect('backend/pages')->with('info','Record Updated Successfully!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

//status update
    public function updateStatus(){
    $status=$_GET['status'];
   if($_GET['status']=='yes'){
      $status='no';

   }
   else if($_GET['status']=='no'){
  $status='yes';
   }
  $data=array('is_active'=>  $status);

    $id=$_GET['u_id'];
 Page::where('id',$id)->update($data);  

//return redirect('backend/pages');
}

//builder

    public function builder(){
       
        return view('backend.builders');
           
}

}