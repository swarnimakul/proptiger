<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Specification;


class SpecificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Properties';
        $specifications =  DB::table('specifications')
                        ->where('specification_id' , '=' , '0')
                        ->get();
       
        return view('backend.specifications', compact('sidebarTab', 'specifications'));
    }

   public function viewFeature($id)
    {
        $sidebarTab = 'Properties';
        $specifications =  DB::table('specifications')
                        ->where('specification_id' , '=' , $id)
                        ->get();
       
       $id = $id;
        return view('backend.specifications', compact('sidebarTab', 'specifications','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $sidebarTab = 'Properties';
         
        return view('backend.addspecifications', compact('sidebarTab'));
    }



    public function addFeature(Request $request,$id)
    {
        $sidebarTab = 'Properties';
         $specifications =  DB::table('specifications')
                        ->where('id' , '=' , $id)
                        ->first();
         $specification_id= $specifications->id;
         $specification_name= $specifications->specification;
        
                      //  print_r($specification_id);die;
        return view('backend.addspecifications', compact('sidebarTab','specification_name','specification_id'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'specification' => 'required',
            ]);
                  
        $alreadyExist = DB::table('specifications')->select('specification')->where('specification' , '=' , $request['specification'])->first();
        if(!empty($alreadyExist)){
            return redirect('backend/add-specifications')->with('info',
           'Already Exist');
        }else{
           
            $specification = new Specification;
            $specification->specification = $request['specification'];
            $specification->specification_id = 0;
            $specification->save();
            return redirect('backend/specifications')->with('info',
           'Record added successfully');
         }  
    }


     public function storeFeature(Request $request)
    {
        $request->validate([
            'specification' => 'required',
            ]);
        $id =$request['id'];
        // echo $request['id'];die;         
        $alreadyExist = DB::table('specifications')
                    ->select('specification')
                    ->where('specification' , '=' , $request['specification'])
                    ->where('specification_id' , '=' , $request['id'])
                    ->first();
        if(!empty($alreadyExist)){
            return redirect('backend/add-feature/'.$id)->with('info',
           'Already Exist');
        }else{
           
            $specification = new Specification;
            $specification->specification = $request['specification'];
            $specification->specification_id=$request['id'];
            $specification->feature = $request['feature'];
           
            $specification->save();
            return redirect('backend/view-feature/'.$id)->with('info',
           'Record added successfully');
         }  
    }


    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $sidebarTab = 'Properties';
         $specifications =  DB::table('specifications')
                        ->where('id' , '=' , $id)
                        ->first();
        
        
                      //  print_r($specification_id);die;
        return view('backend.updatespecification', compact('sidebarTab','specifications'));
    }

  //update specification
    public function updateSpecification(Request $request,$id){
       $sidebarTab = 'Properties';
        $request->validate([
            'specification' => 'required',
            ]);
                  
        $alreadyExist = DB::table('specifications')->select('specification')->where('specification' , '=' , $request['specification'])->where('id','!=',$id)->first();
        if(!empty($alreadyExist)){
            return redirect('backend/update-specification/'.$id)->with('info',
           'Specification Already Exist');
        }else{
         $specifications =  DB::table('specifications')
                        ->where('id' , '=' , $id)
                        ->update(['specification'=>$request->specification]);
return redirect('backend/specifications')->with('info',
'Specification Updated successfully');
}
    } 

//edit feature
public function editFeature($id){
         $sidebarTab = 'Properties';
        
       
       
          $features =  DB::table('specifications')
                        ->where('id' , '=' , $id)
                        ->first();       
          $specification=DB::table('specifications')
                    ->where('id' , '=' , $features->specification_id)->first();    
         $specification_id= $specification->id;
        
         $specification_name= $specification->specification; 

        return view('backend.updatespecification', compact('sidebarTab','specification_name','specification_id','features'));

}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    //update feature
    public function updateFeature(Request $request,$id)
    {
       $sidebarTab = 'Properties';
        $message="Record updated successfully!";
         $request->validate([
            'specification' => 'required',
            ]);
        $id =$request['id']; 
        $specification = Specification::find($id);
        $alreadyExist = Specification::where('id','!=',$request['id'])->where('specification',$request->specification)->where('specification_id',$specification->specification_id)->first();
          if(!empty($alreadyExist)){
            return redirect('backend/update-feature/'.$id)->with('info',
           'Already Exist');
            }else{
           
            
            $specification->specification = $request['specification'];
           
            $specification->feature = $request['feature'];
           //dd($specification);
            $specification->update();
            $specifications =  DB::table('specifications')
                        ->where('specification_id' , '=' ,  $specification->specification_id)
                        ->get();
       
       $id = $id;
        return view('backend.specifications', compact('sidebarTab', 'specifications','id','message'));
         }  
      }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

    public static function  getProjectCount($id){
        $NoOfProjects=0;
          if(!empty($id)){
            $NoOfProjects=DB::table('subdomain_projects')
            ->where('subdomain_id',$id)->count();
          }
          
            echo $NoOfProjects;
    }

    public  function  getProject($id){
         $sidebarTab = 'Subdomains';
            $subdomainstitle=DB::table('subdomains')
            ->select('id','name as subd_name')->where('id',$id)->first();
            $projects=DB::table('subdomain_projects')
           ->leftjoin('subdomains', 'subdomains.id', '=', 'subdomain_projects.subdomain_id')
             ->select('subdomain_projects.name','subdomain_projects.image','subdomain_projects.id as spid','subdomains.id as sid','subdomains.url')
             ->where('subdomain_id',$id)->get();
        return view('backend.subdomainProject', compact('sidebarTab', 'projects','subdomainstitle'));
       
    }

    //edit subdomain-project
  public function editproject($id){
    $sidebarTab = 'Subdomains';
    $subdomainsprojects = DB::table('subdomain_projects')             ->where('id',$id)->first();

    return view('backend.addSubProject', compact('sidebarTab','subdomainsprojects')); 
  }


//update subdomain_project
public function updateproject(Request $request,$id){
$sidebarTab = 'Subdomains';

if($request->hasFile('image')){
$filename=SubdomainProject::where('id',$id)->first();
if($filename!=null){
$file_path=public_path('upload\subdomain_projects\\'.$filename->image);

if(is_file($file_path)) {

unlink($file_path);
}
}
$extension=$request->file('image')->getClientOriginalExtension();
$logo=$request['name'].'.'.$extension;
$request->file('image')->move(public_path('upload/subdomain_projects'),$logo);

$subdomainproject = SubdomainProject::find($id);
  $subdomainproject->name=$request->name;
  $subdomainproject->image=$logo;
  $subdomainproject->website=$request->url;
  $subdomainproject->update();
  //dd($request['url']);
            }
            else{
$subdomainproject = SubdomainProject::find($id);
  $subdomainproject->name=$request->name;
  $subdomainproject->website=$request->url;
  $subdomainproject->update();
}
$sid=$request->sid;
  return redirect('backend/subdomain-projects/'.$sid)->with('info','Record Updated Successfully!');
}

//status update
public function updateSubdomainStatus(){
   $is_verified=$_GET['is_verified'];
   if($_GET['is_verified']==1){
        $is_verified=0;
   }
   else if($_GET['is_verified']==0){
        $is_verified=1;
   }
   $data=[
    'is_verified'=>  $is_verified,
  ];

    $id=$_GET['u_id'];
 Subdomain::where('id',$id)->update($data);  
}


//Get-subdomain
public function getSubdomains(){
  $sidebarTab = 'Subdomains';
        $subdomains = DB::table('subdomains')
                        ->select('name','id')
                        ->orderBy('name', 'Asc')
                        ->get();
          
        return view('backend.addsubdomainprice', compact('sidebarTab','subdomains'));
}

//add-subdomain-price
public function addSubdomainPrice(Request $request)
  { 
          
      $sidebarTab = 'Subdomains';
         $request->validate([
            'subdomain_id' => 'required',
         ]);
         //save price list array 
       $subdomainpricearray=array();
       
       
 //for multiple record entry
    for($i=0;$i<count($request['unit_size']);$i++){
              $subdomainprice = new SubdomainPrice;
              $subdomainprice->subdomain_id = $request->subdomain_id;
              $subdomainprice->accomodation_type = $request['accomadation_type'][$i];
              $subdomainprice->unit_size   = $request['unit_size'][$i];
              $subdomainprice->basic_price = $request['basic_price'][$i];
              $subdomainprice->total_price = $request['total_price'][$i];

             $subdomainpricearray[] = $subdomainprice->attributesToArray();
                  }
        ///fetch list
      foreach ($subdomainpricearray as $list) {
         SubdomainPrice::insert($list); 
      }
 
      return redirect('backend/add-subdomain-price')->with('message','success');


}


 

}