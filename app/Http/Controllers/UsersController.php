<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function signUp() {
		// Validate the form
		$validator = $this->validate(request(), [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'phone' => 'required|numeric|min:10'
		]);

    	// Create the user and set is_verified flag to true
    	$user = User::create(
            array_merge(
                request(['name', 'email', 'password', 'phone']), 
                ['is_verified'=> 1])
        );

    	// Sign in the user
    	auth()->login($user);

    	// Return the response
		//return response()->json($response);
    }

    public function signIn() {
    	$request = request()->all();
    	$rememberMe = ! empty(request('rememberMe')) ? 1 : 0;

    	if ( auth()->attempt([
    			'email' => $request['email'], 
    			'password' => $request['password']
    		], $rememberMe) && auth()->user()->is_verified ) {

			// Saving users legal query in session
			Session::put('qryParams.legalQry', $request['legalQry']);

    		// Authentication passed...
            //return redirect()->intended('dashboard');
            $response = [
				'message' => 'User verified successfully.',
				'status'  => true
			];
    	} else {
			$response = [
				'message' => 'User verification failed.',
				'status'  => false
			];
		}

    	return response()->json($response);
    }

    public function signOut() {
        auth()->logout();

        // Flushing all session
        Session::flush();

        // redirect to home page
        return redirect()->home();
    }
}
