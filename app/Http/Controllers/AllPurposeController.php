<?php
namespace App\Http\Controllers;

use App\UserQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AllPurposeController extends Controller
{
	public function sendOtp() {
		$request = request()->all();

		// Validate the form
		$validator = $this->validate(request(), [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'phone' => 'required|numeric|min:10'
		]);

		$otp = $this->randomDigit();
		Session::put('OTP', $otp);
		$phone = $request['phone'];
		$url = 'http://bhashsms.com/api/sendmsg.php';
		$post = [
			'user' => 'dztultrans',
			'pass' => '123',
			'sender' => 'RERAIN',
		//	'phone' => '8010179239',
			'phone' => $phone,
			'text' => $otp . ' is your One Time Password for phone number verification.',
			'priority' => 'ndnd',
			'stype' => 'normal'
		];

		// CURL call to send OTP on given phone number
		$result = $this->callCurl( $url, $post );

		$response = [
			'message' => 'Something went wrong. Please try again or contact to administrator.',
			'status'  => false
		];

		if( ! empty($result) ) {
			$response = [
				'message' => 'Please enter verification code (OTP) sent to ' . $phone,
				'status'  => true
			];
		}

		return response()->json($response);
	}

	public function verifyOtp() {
		$request = request()->all();
		$otp = Session::get('OTP');

		if ( $otp == $request['otp'] ) {
				$response = [
				'message' => 'Phone number verified successfully.',
				'status'  => true
			];

			// Creating new user
			app('\App\Http\Controllers\UsersController')->signUp();

			// Saving users legal query in session
			Session::put('qryParams.legalQry', $request['legalQry']);

			//Removing session variable
            Session::pull('OTP');
		} else {
			$response = [
				'message' => 'Either OTP has expired or invalid.',
				'status'  => false
			];
		}

		return response()->json($response);
	}

	public function randomDigit($length = 5) {
		$range = range(0, 9);
		shuffle($range);

		for ($i=0; $i < $length; $i++) { 
			$randomDigit[] = $range[mt_rand(0, 9)];
		}

		return implode('', $randomDigit);
	}

	public function doPayment() {
		$request = request()->all();

		$legalQry = $request['legalQry'];
		$builderId = $request['builderId'];
		$projectId = $request['projectId'];

        Session::put('qryParams.legalQry', $legalQry);
        Session::put('qryParams.builderId', $builderId);
        Session::put('qryParams.projectId', $projectId);

        // Insert query in DB
        $userQuery = new UserQuery;

        $userQuery->user_id = auth()->user()->id;
        $userQuery->query = Session::get('qryParams.legalQry');
        $userQuery->state_id = Session::get('qryParams.stateId');
        $userQuery->builder_id = Session::get('qryParams.builderId');
        $userQuery->project_id = Session::get('qryParams.projectId');

        $userQuery->save();

		$response = [
			'message' => 'Please wait while we redirect you to payment gateway...',
			'status'  => true
		];

		return response()->json($response);

		/*$PAYU_BASE_URL = "https://test.payu.in";
		$posted['hash'] = 'ec457d0a974c48d5685a7efa03d137dc8bbde7e3';
		$posted['key'] = 'gtKFFx';
		$posted['txnid'] = 'eCwWELxi';
		$posted['amount'] = '22';
		$posted['productinfo'] = 'Rera Legal Query';
		$posted['firstname'] = 'Akshay';
		$posted['email'] = 'test@gmail.com';
		$posted['phone'] = '9538079055';
		$posted['surl'] = 'http://yoursite.com/payment/success';
		$posted['furl'] = 'http://yoursite.com/payment/failure';
		$hashSequence ="key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
//hash = sha512(gtKFFx|eCwWELxi|22|Rera Legal Query|Akshay|test@gmail.com|||||||||||eCwWELxi)
		if(empty($posted['hash']) && sizeof($posted) > 0) {
			if( empty($posted['key'])
				|| empty($posted['txnid'])
				|| empty($posted['amount'])
				|| empty($posted['firstname'])
				|| empty($posted['email'])
				|| empty($posted['phone'])
				|| empty($posted['productinfo'])
				|| empty($posted['surl'])
				|| empty($posted['furl'])) {
				$formError = 1;
			}
			else
			{
				$hashVarsSeq = explode('|', $hashSequence);
				$hash_string = '';
				foreach($hashVarsSeq as $hash_var) {
					$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
					$hash_string .= '|';
				}
				$SALT = '';
				$hash_string .= $SALT;
				$hash = strtolower(hash('sha512', $hash_string));
				$action = $PAYU_BASE_URL . '/_payment';
			}
		} elseif(!empty($posted['hash'])) {
			$hash = $posted['hash'];
			$action = $PAYU_BASE_URL . '/_payment';
		}

		$responsePayU = $this->callCurl( $action, $posted );*/
		echo '<pre>';print_r($responsePayU);die;
	}


	public function paymentForm() {
		// Merchant key here as provided by Payu
		$MERCHANT_KEY = "gtKFFx"; //Please change this value with live key for production
		$hash_string = '';
		// Merchant Salt as provided by Payu
		$SALT = "eCwWELxi"; //Please change this value with live salt for production

		// End point - change to https://secure.payu.in for LIVE mode
		$PAYU_BASE_URL = "https://test.payu.in";

		$action = '';

		$posted = array();
		if( ! empty($_POST) ) {
		    //print_r($_POST);
		  foreach( $_POST as $key => $value ) {    
		    $posted[$key] = $value; 
			
		  }
		}

		$formError = 0;

		if( empty($posted['txnid']) ) {
		   // Generate random transaction id
		  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		} else {
		  $txnid = $posted['txnid'];
		}
		$hash = '';
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		if( empty($posted['hash']) && sizeof($posted) > 0 ) {
		  if(
		          empty($posted['key'])
		          || empty($posted['txnid'])
		          || empty($posted['amount'])
		          || empty($posted['firstname'])
		          || empty($posted['email'])
		          || empty($posted['phone'])
		          || empty($posted['productinfo'])
		         
		  ) {
		    $formError = 1;
		  } else {
		    
			$hashVarsSeq = explode('|', $hashSequence);
		 
			foreach($hashVarsSeq as $hash_var) {
		      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
		      $hash_string .= '|';
		    }

		    $hash_string .= $SALT;


		    $hash = strtolower(hash('sha512', $hash_string));
		    $action = $PAYU_BASE_URL . '/_payment';
		  }
		} elseif(!empty($posted['hash'])) {
		  $hash = $posted['hash'];
		  $action = $PAYU_BASE_URL . '/_payment';
		}

		return view('paymentform', compact('MERCHANT_KEY', 'txnid', 'hash', 'action', 'formError'));
	}


	public function paymentResponse() {

		return view('paymentresponse');
	}


	public function callCurl( $url, $post ) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}


	public function getPageContent($pagename) {
		$pageData = \App\StaticPage::where('url', '=', $pagename)->first();
		$title = $pageData['title'];
		$content = $pageData['content'];

		return view('staticpage', compact('title', 'content'));
	}


	public function limitWords( $content, $limit = 40 ) {
		return implode(' ', array_slice(explode(' ', strip_tags($content)), 0, $limit));
	}
}
