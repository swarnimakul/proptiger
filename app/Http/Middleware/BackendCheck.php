<?php

namespace App\Http\Middleware;

use Closure;

class BackendCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Role 0:Customer, 1:Admin
        // If the login user is not Admin, redirect with error message
        if( ! auth()->check() || auth()->user()->role === 0 ) {

            return redirect()->route('backendLogin')->with('status', 'Unauthorised access detected!!');
        }

        return $next($request);
    }
}
